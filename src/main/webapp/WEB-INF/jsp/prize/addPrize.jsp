<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="../static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="../lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/style.css" />
<style type="text/css">
	.row{
	margin-left: 15%;
	}
</style>
<title></title>
</head>
<body>
<article class="page-container">
	<form class="form form-horizontal" id="form-prize-add">
		<!-- 奖品名称 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>奖品名称：</label>
			<div class="formControls col-xs-5 col-sm-6">
				<input type="text" class="input-text" value="" placeholder="" id="pName" name="">
			</div>
		</div>
		<!-- 奖品数量 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>奖品数量：</label>
			<div class="formControls col-xs-5 col-sm-6">
				<input type="text" class="input-text" value="" placeholder="" id="pCount" name="">
			</div>
		</div>
		<!-- 奖品等级 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>奖品等级：</label>
			<div class="formControls col-xs-5 col-sm-6">
				<input type="text" class="input-text" value="" placeholder="" id="pLev" name="">
			</div>
		</div>
		<!-- 奖品描述 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>奖品描述：</label>
			<div class="formControls col-xs-5 col-sm-6">
				<textarea id="pDesc" name="" cols="" rows="" class="textarea"  placeholder="" ></textarea>
			</div>
		</div>
		<!-- 添加-->
		<div class="row cl">
			<div class="col-xs-5 col-sm-6 col-xs-offset-4 col-sm-offset-2">
				<font color="#ccc"><div id="msg" style="text-align: center"></div></font>
				<button onClick="addPrize();" type="button" class="btn btn-primary radius r" ><i class="Hui-iconfont">&#xe632;</i> 添加</button>
			</div>
		</div>
	</form>
</article>
<script type="text/javascript" src="../lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="../lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="../static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="../static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="../lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="../lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="../lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="../lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript" src="../lib/webuploader/0.1.5/webuploader.min.js"></script> 
<script type="text/javascript" src="../lib/ueditor/1.4.3/ueditor.config.js"></script> 
<script type="text/javascript" src="../lib/ueditor/1.4.3/ueditor.all.min.js"> </script> 
<script type="text/javascript" src="../lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
//添加
function addPrize(){
	var name=$("#pName").val(); //奖品名称
	var count=$("#pCount").val(); //总数
	var level=$("#pLev").val(); //等级
	var desc=$("#pDesc").val(); //描述
	if(name==null||name==""){
		alert("奖品名称不能为空");
		return;
	}
	if(count==null||count==""){
		alert("奖品数量不能为空");
		return;
	}
	if(level==null||level==""){
		alert("奖品等级不能为空");
		return;
	}
	if(desc==null||desc==""){
		alert("奖品描述不能为空");
		return;
	}
	$("#msg").html("提交中....");
	  $.ajax({  
          type:"POST",  
          url:"addPrize.html",  
          data:{"name":name,"count":count,"level":level,"desc":desc},  
          dataType:"json",  
          success:function(res){
        	  if (res.succ == true) {
  				alert(res.mesg);
  				window.location.href='qryPrize.html';
  		        //获取窗口索引
  		        var index = parent.layer.getFrameIndex(window.name);
  		        //关闭弹出层
  		        parent.layer.close(index);
  			} else {
  				alert(res.mesg);
  			}
          }, 
      });
		$("#msg").html("");
}

</script>
</body>
</html>
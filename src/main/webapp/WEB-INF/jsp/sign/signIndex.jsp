<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="../static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="../lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/style.css" />
<style>
.table th, .table td { 
text-align: center; 
}
</style>
<title>签到名单</title>
</head>
<body>
<div class="page-container">
	<table class="table table-border table-bordered table-bg">
		<thead>
			<tr class="text-c">
				<th><input type="checkbox" name="" value=""></th>
				<th>ID</th>
				<th>微信名</th>
				<th>openid</th>
				<th>头像</th>
				<th>是否可以抽奖</th>
				<th>签到时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody id="QUERY_RESULT">
               <tr><td colspan='20' >查询中,请稍等...</td></tr>
        </tbody>
	</table>
</div>
<script type="text/javascript" src="../lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> 
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script> 
<script type="text/javascript" src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
var list = ${data.list};
var dataRows="";
if(list.length==0){
	$('#QUERY_RESULT').html("<tr><td colspan='20' style='text-alin:center'>暂无数据</td></tr>");
}else{
	var starr ={1:"是",0:"否"};
	for (var i = 0; i < list.length; i++) {
		var row = '<tr>'
		+'<td><input type="checkbox" class="flat" name="checkStatus" id="checkStatus" value="'+list[i].id+'"></input></td>'
		+'<td>'+list[i].id+"</td>"
		+'<td>'+list[i].nickName+"</td>"
		+'<td>'+list[i].openId+"</td>"
		+"<td><image style='width:25px;height:25px' src='"+list[i].headImg+"'/></td>"
		+'<td>'+starr[list[i].state]+"</td>"
		+'<td>'+list[i].signTime+"</td>"
		+'<td>'
		if(list[i].state=="1"){
			row=row+'<a href="javaScript:conform('+"'"+list[i].id+"','"+list[i].state+"'"+')">取消</a>&nbsp;</td>';
		}else{
			row=row+'<a href="javaScript:conform('+"'"+list[i].id+"','"+list[i].state+"'"+')">确认</a>&nbsp;</td>';
		}
		/*+'<a href="javaScript:toInfo('+"'"+list[i].id+"','"+list[i].mno+"'"+')">删除</a>&nbsp;' */
		+'</tr>';
		dataRows = dataRows + row;
	}
	$('#QUERY_RESULT').html(dataRows);
	
}
//删除评分
function delScore(id){
	if (!confirm('确定要删除该评分吗?')) {
		return;
	}
	$.ajax({
		type : 'post',
		data : {"id":id},
		dataType : 'json',
		url : "delScore.html",
		success : function(res) {
		  if (res.succ == true) {
				alert(res.mesg);
			} else {
				alert(res.mesg);
			}
		},
		error : function() {
			alert("网络异常！");
		}
	});
}
function conform(id,state){
	$.ajax({  
        type:"POST",  
        url:"toConfirm.html",  
        data:{"id":id,"state":state},  
        dataType:"json",  
        success:function(res){
      	  if (res.succ == true) {
			 	alert(res.mesg);
				 window.location.href='qrySign.html';
			} else {
				alert(res.mesg);
			}
        }, 
    });
}





</script>
</body>
</html>
<%@page import="com.alibaba.fastjson.JSONArray"%>
<%@page import="com.alibaba.fastjson.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	JSONObject object = (JSONObject) request.getAttribute("data");
	JSONArray array = object.getJSONArray("list");
%>
<!doctype html>
<html>
<head>
	<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
	<title>节目列表</title>
	<style>
		.ruleDiv{
			margin-top: 15px;
			margin-left: 15px;
			margin-right: 15px;
		}
		.listDiv{
			margin-top: 15px;
			margin-left: 15px;
			margin-right: 15px;
			
		}
		.h{
			text-align: center;
			font-size: 14px;
			color: #333;
			font-family: "MicrosoftYaHei";
			width: 50%;
		}
		.tab{
			width: 100%;
		}
		td{
			font-size: 12px;
			color: #333;
			font-family: "MicrosoftYaHei";
			text-align: center;
			line-height: 15px;
			padding-left:8%;
			padding-right: 8%;
		}
	</style>
</head>

<body style="background-color: #fff;margin: 0px" >
	<div style="width: 100%">
		<div id="banner">
			<img  src="../images/meet.png" style="width: 100%;height: 100%">
		</div>
		<div id="content" style="">
			<div class="ruleDiv">
				<span style="font-size: 14px;color: #333;border: none" >入场时间地点</span>
				<div style="font-size: 12px;color: #333;margin-top: 10px;line-height: 20px">
					2018年2月3日 12:40~21:00华彬费尔蒙酒店-朝阳区建国门外大街永安里东里8号华彬费尔蒙酒店（秀水街对面）三层金鱼胡同会议厅。
				</div>
			</div>
			<div class="listDiv">
				<table class="tab">
					<thead style="height: 35px;">
						<tr class="" style="background-color: #f1f1f1;">
							<th class="h">节目</th>
							<th class="h">时间</th>
						</tr>
					</thead>
					 <tbody id="QUERY_RESULT">
					 	<%for(int i = 0; i < array.size(); i++) {
					 		JSONObject jsonObject = array.getJSONObject(i);
					 	%>
					 	<tr class="con">
				 			<td>
					 			<div style="border-bottom: 1px solid #ddd;">
					 				<span><%=jsonObject.getString("proName") %></span>
					 			</div>
				 			</td>
							<td>
								<div style="border-bottom: 1px solid #ddd;">
					 				<span><%=jsonObject.getString("time") %></span>
					 			</div>
					 		</td>
						</tr>
					 	<%} %>
			         </tbody>
				</table>
			</div>
		</div>
	</div>
<script type="text/javascript" src="../lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript">
	
</script>
</body>

</html>
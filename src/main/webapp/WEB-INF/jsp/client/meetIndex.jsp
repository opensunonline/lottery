<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
	<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
	<title>好再莱年会活动</title>
	<style>
		.btn{
			  background: -webkit-linear-gradient(left top, #409eff, #78d1ff); /* Safari 5.1 - 6.0 */
			  background: -o-linear-gradient(bottom right, #409eff, #78d1ff); /* Opera 11.1 - 12.0 */
			  background: -moz-linear-gradient(bottom right, #409eff, #78d1ff); /* Firefox 3.6 - 15 */
			  background: linear-gradient(to bottom right, #409eff, #78d1ff); /* 标准的语法 */
			  font-family: "MicrosoftYaHei";
			  font-size: 12px;
			  color: #fff;
			  height: 35px;
			  margin-top:10px;
			  border-radius: 40px;
			  padding-top: 2px;
			  width: 44%;
			  border: none;
		}
		.barrage{
			width: 99%;
			height:115px;
			border: none;
			background: #dfdfdf;
			overflow:hidden;
			font-size: 16px;
			font-family: MicrosoftYaHei;
			-webkit-tap-highlight-color:rgba(0,0,0,0);
		}
		.sendBtn{
			 background: -webkit-linear-gradient(left top, #409eff, #78d1ff); /* Safari 5.1 - 6.0 */
			  background: -o-linear-gradient(bottom right, #409eff, #78d1ff); /* Opera 11.1 - 12.0 */
			  background: -moz-linear-gradient(bottom right, #409eff, #78d1ff); /* Firefox 3.6 - 15 */
			  background: linear-gradient(to bottom right, #409eff, #78d1ff); /* 标准的语法 */
			  font-family: "microsoft yahei";
			  font-size: 16px;
			  color: #fff;
			  height: 45px;
			  margin-top:25px;
			  border-radius: 40px;
			  width: 80%;
			  border: none;
		}
		.sendDiv{
			text-align: center;
			
		}
		.ruleDiv{
			margin-top: 25px;
			margin-left: 15px;
			margin-right: 15px;
		}
		.editDiv{
			margin-left: 15px;
			margin-right: 15px;
			margin-top: 10px;
			background: #dfdfdf;
			padding: 15px;
		}
	</style>
</head>

<body style="background-color: #fff;margin: 0px" >
	<div style="width: 100%">
		<div id="banner" style="">
			<img  src="../images/meet.png" style="width: 100%;height: 100%">
		</div>
		<div id="content" style="">
			<div>
				<button class="btn" style="margin-left: 15px;" onclick="prize();" >我的奖品</button>
				<button class="btn" style="margin-right: 15px;float: right;" onclick="qryPro();">节目列表</button>
			</div>
			<div class="editDiv">
				<textarea type="text" oninput="onInput();" id="barrage" class="barrage" placeholder="输入弹幕 "></textarea>
				 <span id="span_meg" style="color: red;float: right;font-size: 10px" ></span>
			</div>
			<div class="sendDiv" >
					<button class="sendBtn" onclick="sendBarrage()">发送弹幕</button>
			</div>
			<div class="ruleDiv">
				<span style="font-size: 14px;color: #333;" >规则说明</span>
				<div style="font-size: 11px;color: #666;margin-top: 15px;line-height: 20px">
					1.这就是一个游戏游戏很少见啊哈就是军飞是发多久闪电发货数据返回到斯维尔我百度
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../lib/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	//查询节目列表
	function qryPro(){
		location.href="toPro.html";
	}
	function prize(){
		location.href="toPrize.html";
	}
	
	function sendBarrage() {
		var barrage = $("#barrage").val(); //奖品名称
		if(barrage == null || barrage == '') {
			alert("请输入弹幕信息");
			return;
		}
		 $.ajax({  
	          type:"POST",  
	          url:"sendBarrage.html",  
	          data:{'content': barrage},  
	          dataType:"json",  
	          success:function(res){
	        	  if (res.succ == true) {
	        		
	  				$("#barrage").val('')
	  			} else {
	  				alert(res.mesg);
	  			}
	          }, 
	      });
	}
	function onInput() {
		 var cont = $("#barrage").val()
	    var leng = cont.length;
	    leng = parseInt(leng);
	    var leng2 = parseInt(20 - leng);
	    if (leng < 20) {
	        $("#span_meg").text("您还可以输入" + leng2 + "个字");
	    }
	    else {
	        var v1 = $("#barrage").val();
	        $("#barrage").val(v1.substr(0, 20))
	        $("#span_meg").text("您还可以输入0个字");
	    }
	}
</script>
</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="../static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="../lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/style.css" />
<style>
.table th, .table td { 
text-align: center; 
}
</style>
<title>节目列表</title>
</head>
<body> 
<nav class="breadcrumb">
	<div class="cl"> 
			<a class="btn btn-primary radius r" style="line-height:1.6em;margin:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
			<a class="btn btn-primary radius r" onclick="Hui_admin_tab(this)" style="line-height:1.6em;margin:3px" data-title="添加节目" data-href="programMg/toAddProgram.html" href="javascript:void(0)"><i class="Hui-iconfont">&#xe600;</i> 添加</a>
	</div>
</nav>

<div class="page-container">
	<table class="table table-border table-bordered table-bg">
		<thead>
			<tr class="text-c">
				<th><input type="checkbox" name="" value=""></th>
				<th>ID</th>
				<th>节目名称</th>
				<th>表演者</th>
				<th>表演时间</th>
				<th>节目描述</th>
				<th>创建时间</th>
				<th>操作</th>
			</tr>
		</thead>
		 <tbody id="QUERY_RESULT">
               <tr><td colspan='20' >查询中,请稍等...</td></tr>
         </tbody>
	</table>
</div>
<script type="text/javascript" src="../lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="../lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="../static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="../static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="../lib/My97DatePicker/4.8/WdatePicker.js"></script> 
<script type="text/javascript" src="../lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="../lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
var list = ${data.list};
var dataRows="";
$(document).ready(function(){  
   query();
}); 
function query(){
	if(list.length==0){
		$('#QUERY_RESULT').html("<tr><td colspan='20' style='text-alin:center'>暂无数据</td></tr>");
	}else{
		for (var i = 0; i < list.length; i++) {
			var row = '<tr>'
			+'<td><input type="checkbox" class="flat" name="checkStatus" id="checkStatus" value="'+list[i].id+'"></input></td>'
			+'<td>'+list[i].id+"</td>"
			+'<td>'+list[i].proName+"</td>"
			+'<td>'+list[i].performer+"</td>"
			+'<td>'+list[i].time+"</td>"
			+'<td>'+list[i].proDesc+"</td>"
			+'<td>'+list[i].ct+"</td>"
			+'<td>'
			/* +'<a href="javaScript:toUpd('+"'"+list[i].id+"','"+list[i].mno+"'"+')">修改</a>&nbsp;' */
			+"<a href='javaScript:delProgram("+list[i].id+")'>删除</a>&nbsp;"
			+'</td></tr>';
			dataRows = dataRows + row;
		}
		$('#QUERY_RESULT').html(dataRows);
	}
}

//删除
function delProgram(id){
	if (!confirm('确定要删除该节目吗?')) {
		return;
	}
	$.ajax({
		type : 'post',
		data : {"id":id},
		dataType : 'json',
		url : "delProgram.html",
		success : function(res) {
		  if (res.succ == true) {
				alert(res.mesg);
				
			} else {
				alert(res.mesg);
			}
		},
		error : function() {
			alert("网络异常！");
		}
	});
}




</script>
</body>
</html>
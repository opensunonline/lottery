package com.pay200.cpc.msg.in.event;

import com.pay200.cpc.msg.in.InMsg;

public abstract class EventInMsg extends InMsg {

	protected String event;

    public EventInMsg(String toUserName, String fromUserName, Integer createTime, String msgType, String event)
    {
        super(toUserName, fromUserName, createTime, msgType);
        this.event = event;
    }

    public String getEvent()
    {
        return event;
    }

    public void setEvent(String event)
    {
        this.event = event;
    }
	
}

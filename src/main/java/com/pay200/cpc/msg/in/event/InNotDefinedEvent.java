package com.pay200.cpc.msg.in.event;

/**
 * 没有找到适配类型时的事件
 * @author yangyang.zhang
 * @Package com.pay200.cpc.msg.in.event 
 * @Date 2018年1月11日 上午11:48:07 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
public class InNotDefinedEvent extends EventInMsg {
    public InNotDefinedEvent(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
        super(toUserName, fromUserName, createTime, msgType, event);
    }
}

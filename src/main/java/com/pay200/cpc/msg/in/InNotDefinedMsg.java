package com.pay200.cpc.msg.in;

/**
 * 没有找到对应的消息类型
 * @author yangyang.zhang
 * @Package com.pay200.cpc.msg.in 
 * @Date 2018年1月11日 上午11:47:30 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
public class InNotDefinedMsg extends InMsg {
    public InNotDefinedMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }
}


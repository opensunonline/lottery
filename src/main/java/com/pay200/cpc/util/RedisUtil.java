package com.pay200.cpc.util;

import java.util.ResourceBundle;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {

	private static JedisPoolConfig config = new JedisPoolConfig();
	private static ResourceBundle bundle = ResourceBundle.getBundle("weixin");
	private static JedisPool pool = null; 
	private static String host = null;
	private static String port = null;
	private static String password = null;
	private static int library = 5;
	static{
		 host = bundle.getString("jedis_host"); //主机地址
		 port = bundle.getString("jedis_port"); //主机地址
		 password = bundle.getString("jedis_password"); //主机地址
		 config.setMaxIdle(100); //最大空闲连接数, 应用自己评估，不要超过ApsaraDB for Redis每个实例最大的连接数
		 config.setMaxTotal(500); //最大连接数, 应用自己评估，不要超过ApsaraDB for Redis每个实例最大的连接数
		 config.setTestOnBorrow(false);
		 config.setTestOnReturn(false);
		 pool = new JedisPool(config, host, Integer.valueOf(port), 3000, password);   //创建redis链接池对象
	}
	

	public static void set(String key, String value, String seconds) throws Exception {
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			jedis.set(key, value);
			if(seconds != null && Integer.valueOf(seconds) > 0){
				jedis.expire(key, Integer.valueOf(seconds));
			}
		}finally {
			if (jedis != null) {
		        jedis.close();
		    }
		}
		
	}

	public static String get(String key) throws Exception {
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			return jedis.get(key);
		}finally {
			if (jedis != null) {
		        jedis.close();
		    }
		}
		
	}
	
	public static String getAndSet(String key, String seconds) throws Exception {
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			String value = jedis.get(key);
			jedis.set(key, value);
			if(seconds != null && Integer.valueOf(seconds) > 0){
				jedis.expire(key, Integer.valueOf(seconds));
			}
			return value;
		}finally {
			if (jedis != null) {
		        jedis.close();
		    }
		}
	}
	
	public static String getAndDel(String key) throws Exception {
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			String value = jedis.get(key);
			jedis.del(key);
			return value;
		}finally {
			if (jedis != null) {
		        jedis.close();
		    }
		}
	}
	
}

package com.pay200.cpc.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalConfiguration.Node;

import com.pay200.cpc.controller.MsgPubController;
import com.pay200.cpc.msg.in.InMsg;
import com.pay200.cpc.msg.in.InTextMsg;
import com.pay200.cpc.msg.in.event.InNotDefinedEvent;
import com.pay200.cpc.msg.in.event.InQrCodeEvent;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLParse {
	private static Logger logger = LoggerFactory.getLogger(MsgPubController.class);
	private String xml;
	public static Map<String, String> maps = new HashMap<String, String>();
	public XMLParse(){}

	public XMLParse(String xml){
		this.xml = xml;
	}

	public static Map<String, String> parse(String xml) {
		try{
			InputStream in = new ByteArrayInputStream(xml.getBytes("UTF-8"));  
			XMLConfiguration configuration = new XMLConfiguration();
			configuration.load(in);
			Node node = configuration.getRoot();
			List children = node.getChildren();
			for(int i=0;i<children.size();i++){
				Node node2 = (Node) children.get(i);
				String name = node2.getName();
				String values = (String) node2.getValue();
				maps.put(name, values);
			}
			return maps;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static InMsg doParse(String xml) {
		Map<String, String> map = parse(xml);
		String toUserName = map.get("ToUserName");
		String fromUserName = map.get("FromUserName");
		Integer createTime = Integer.valueOf(map.get("CreateTime"));
		String msgType = map.get("MsgType");
		if("text".equals(msgType))
			return parseInTextMsg(map, toUserName, fromUserName, createTime, msgType);
		if ("image".equals(msgType))
			return null;
		if ("voice".equals(msgType))
			return null;
		if ("video".equals(msgType))
			return null;
		if ("shortvideo".equals(msgType))
			return null;
		if ("location".equals(msgType))
			return null;
		if ("link".equals(msgType))
			return null;
		if ("event".equals(msgType))
			return parseInEvent(map, toUserName, fromUserName, createTime, msgType);
		
		return null;
	}
	//解析事件
	private static InMsg parseInEvent(Map<String, String> map, String toUserName, String fromUserName,
			Integer createTime, String msgType) {
		String event = map.get("Event");
		String eventKey = map.get("EventKey");
		logger.info("parse=  event==" + event + "==eventKey==" + eventKey);
		// 扫描带参数二维码事件之一		1: 用户未关注时，进行关注后的事件推送
		String ticket = map.get("Ticket");
		if ("subscribe".equals(event) && !StringUtils.isEmpty(eventKey) && eventKey.startsWith("qrscene_")) {
			InQrCodeEvent e = new InQrCodeEvent(toUserName, fromUserName, createTime, msgType, event);
			e.setEventKey(eventKey);
			e.setTicket(ticket);
			return e;
		}
		// 扫描带参数二维码事件之二		2: 用户已关注时的事件推送
		if ("SCAN".equals(event)) {
			InQrCodeEvent e = new InQrCodeEvent(toUserName, fromUserName, createTime, msgType, event);
			e.setEventKey(eventKey);
			e.setTicket(ticket);
			return e;
		}
		InNotDefinedEvent e = new InNotDefinedEvent(toUserName, fromUserName, createTime, msgType, event);
		return e;
	}
	//解析文本
	public static InMsg parseInTextMsg(Map<String, String> map, String toUserName, String fromUserName, Integer createTime, String msgType) {
		InTextMsg inTextMsg = new InTextMsg(toUserName, fromUserName, createTime, msgType);
		inTextMsg.setContent(map.get("Content"));
		inTextMsg.setMsgId(map.get("MsgId"));
		return inTextMsg;
	}
}

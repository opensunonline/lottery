package com.pay200.cpc.util.wxsign;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

/**
 * 微信消息推送签名验证
 * @author yangyang.zhang
 *
 */
public class SignatureCheckKit {
		
		public static final SignatureCheckKit me = new SignatureCheckKit();
		
		/**
		 * </pre>
		 * @param token 
		 * @param signature 微信加密签名
		 * @param timestamp 时间戳
		 * @param nonce 随机字符串
		 * @return {boolean}
		 */
		public boolean checkSignature(String token, String signature, String timestamp, String nonce) {
			String array[] = {token, timestamp, nonce};
			Arrays.sort(array);
			String tempStr = new StringBuilder().append(array[0] + array[1] + array[2]).toString();
			tempStr = HashKit.sha1(tempStr);
			return tempStr.equalsIgnoreCase(signature);
		}
		
		public boolean checkSignature(HttpServletRequest request, String token) {
	        return checkSignature(token, request.getParameter("signature"), request.getParameter("timestamp"), request.getParameter("nonce"));
		}
	}
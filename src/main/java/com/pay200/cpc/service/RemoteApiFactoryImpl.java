package com.pay200.cpc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import microservice.api.ServiceApi;
@Service
public class RemoteApiFactoryImpl implements RemoteApiFactory {

	@Autowired
	@Qualifier("msgApi")
	private ServiceApi msgApi;

	public ServiceApi getMsgApi() {
		return msgApi;
	}
	public void setMsgApi(ServiceApi msgApi) {
		this.msgApi = msgApi;
	}
}

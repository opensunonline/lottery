package com.pay200.cpc.service;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pay200.cpc.util.wxsign.SignatureCheckKit;

/**
 * 微信推送验证
 * 
 * @author yangyang.zhang
 *
 */
@Service
public class WxMsgUtilServiceImpl implements WxMsgUtilService{

	private static Logger logger = LoggerFactory.getLogger(WxMsgUtilServiceImpl.class);
	private static ResourceBundle bundle = ResourceBundle.getBundle("weixin");

	/**
	 * 是否为开发者中心保存服务器配置的请求
	 * 
	 * @param request
	 * @return
	 */
	public boolean isConfigServerRequest(HttpServletRequest request) {
		return !StringUtils.isEmpty(request.getParameter("echostr"));
	}

	/**
	 * 检测签名
	 * 
	 * @param request
	 * @return
	 */
	public boolean checkSignature(HttpServletRequest request) {
		logger.info("2=============>");
		String token = bundle.getString("token");
		String appId = bundle.getString("appId");
		String signature = request.getParameter("signature");
		String timestamp = request.getParameter("timestamp");
		String nonce = request.getParameter("nonce");
		logger.info("appId=[" + appId + "]=====" + "signature=[" + signature + "]=====" + "timestamp=[" + timestamp + "]=====" + "nonce=[" + nonce + "]");
		try {
			if (StringUtils.isEmpty(appId) || StringUtils.isEmpty(signature) || StringUtils.isEmpty(timestamp)
					|| StringUtils.isEmpty(nonce)) {
				return false;
			}
			if (SignatureCheckKit.me.checkSignature(token, signature, timestamp, nonce)) {
				return true;
			} else {
				logger.error("check signature failure: " + " appId = " + appId + " signature = " + signature
						+ " timestamp = " + timestamp + " nonce = " + nonce + " token = " + token);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("check signature failure: " + " appId = " + appId + " signature = " + signature
					+ " timestamp = " + timestamp + " nonce = " + nonce + " token = " + token, e);
			return false;
		}
	}

	/**
	 * 配置开发者中心微信服务器所需的 url 与 token
	 * 
	 * @param request
	 * @param response
	 */
	public void configServer(HttpServletRequest request, HttpServletResponse response) {
		// 通过 echostr 判断请求是否为配置微信服务器回调所需的 url 与 token
		String token = bundle.getString("token");
		String appId = request.getParameter("appId");
		String echostr = request.getParameter("echostr");
		String signature = request.getParameter("signature");
		String timestamp = request.getParameter("timestamp");
		String nonce = request.getParameter("nonce");
		try {
			
			boolean isOk = SignatureCheckKit.me.checkSignature(token, signature, timestamp, nonce);
			if (isOk) {
				response.getWriter().write(echostr);
				response.getWriter().flush();
				response.getWriter().close();
			} else {
				logger.error("验证失败：configServer");
			}
		} catch (Exception e) {
			logger.error("验证异常：configServer" + " appId = " + appId + " token = " + token + " signature = " + signature
					+ " timestamp = " + timestamp + " nonce = " + nonce, e);
		}
	}

	/**
	 * 是否为开发者中心保存服务器配置的请求
	 * 
	 * @param request
	 * @return
	 */
	public boolean isConfigServerRequest(String echostr) {
		return !StringUtils.isEmpty(echostr);
	}

	/**
	 * 检测签名
	 * 
	 * @param request
	 * @return
	 */
	public boolean checkSignature(String appId, String signature, String timestamp, String nonce) {
		String token = null;
		try {
			if (StringUtils.isEmpty(appId) || StringUtils.isEmpty(signature) || StringUtils.isEmpty(timestamp)
					|| StringUtils.isEmpty(nonce)) {
				return false;
			}
			token = bundle.getString("token");
			if (SignatureCheckKit.me.checkSignature(token, signature, timestamp, nonce)) {
				return true;
			} else {
				logger.error("check signature failure: " + " appId = " + appId + " signature = " + signature
						+ " timestamp = " + timestamp + " nonce = " + nonce + " token = " + token);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("check signature failure: " + " appId = " + appId + " signature = " + signature
					+ " timestamp = " + timestamp + " nonce = " + nonce + " token = " + token, e);
			return false;
		}
	}

	/**
	 * 配置开发者中心微信服务器所需的 url 与 token
	 * 
	 * @param request
	 * @param response
	 */
	public String configServer(String appId, String echostr, String signature, String timestamp, String nonce) {
		// 通过 echostr 判断请求是否为配置微信服务器回调所需的 url 与 token
		String token = null;
		try {
			token = bundle.getString("token");
			boolean isOk = SignatureCheckKit.me.checkSignature(token, signature, timestamp, nonce);
			if (isOk) {
				return echostr;
			} else {
				logger.error("验证失败：configServer");
				return null;
			}
		} catch (Exception e) {
			logger.error("验证异常：configServer" + " appId = " + appId + " token = " + token + " signature = " + signature
					+ " timestamp = " + timestamp + " nonce = " + nonce, e);
		}
		return null;
	}

}

package com.pay200.cpc.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface WxMsgUtilService {


	/**
	 * 是否为开发者中心保存服务器配置的请求
	 * 
	 * @param request
	 * @return
	 */
	public boolean isConfigServerRequest(HttpServletRequest request);

	/**
	 * 检测签名
	 * 
	 * @param request
	 * @return
	 */
	public boolean checkSignature(HttpServletRequest request) ;

	/**
	 * 配置开发者中心微信服务器所需的 url 与 token
	 * 
	 * @param request
	 * @param response
	 */
	public void configServer(HttpServletRequest request, HttpServletResponse response) ;

	/**
	 * 是否为开发者中心保存服务器配置的请求
	 * 
	 * @param request
	 * @return
	 */
	public boolean isConfigServerRequest(String echostr) ;

	/**
	 * 检测签名
	 * 
	 * @param request
	 * @return
	 */
	public boolean checkSignature(String appId, String signature, String timestamp, String nonce) ;

	/**
	 * 配置开发者中心微信服务器所需的 url 与 token
	 * 
	 * @param request
	 * @param response
	 */
	public String configServer(String appId, String echostr, String signature, String timestamp, String nonce) ;

}

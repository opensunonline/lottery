package com.pay200.cpc.service;

public interface RedisService {

	public String crtKey(String appId);
	public void set(String key, String value, String seconds) throws Exception;
	public String get(String key) throws Exception;
	public String getAndSet(String key, String seconds) throws Exception;
	public String getAndDel(String key) throws Exception;
	public void del(String... key) throws Exception;
}

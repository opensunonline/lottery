package com.pay200.cpc.service;

import java.util.ResourceBundle;

import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Service
public class RedisServiceImpl implements RedisService{
	private static JedisPoolConfig config = new JedisPoolConfig();
	private static ResourceBundle bundle = ResourceBundle.getBundle("weixin");
	private static JedisPool pool = null;
	private static String host = null;
	private static String port = null;
	private static String password = null;
	private static int library = 5;		//redis库
	public void init() {
		if (pool == null) {
			host = bundle.getString("jedis_host");
			port = bundle.getString("jedis_port");
			password = bundle.getString("jedis_password");
			config.setMaxIdle(100); // 最大空闲连接数, 应用自己评估，不要超过ApsaraDB for
									// Redis每个实例最大的连接数
			config.setMaxTotal(500); // 最大连接数, 应用自己评估，不要超过ApsaraDB for
										// Redis每个实例最大的连接数
			config.setTestOnBorrow(false);
			config.setTestOnReturn(false);
			pool = new JedisPool(config, host, Integer.valueOf(port), 3000, password); // 创建redis链接池对象
		}
	}

	public String crtKey(String appId) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("wx_").append(appId).append("_access_token");
		return buffer.toString();
	}
	
	@Override
	public void set(String key, String value, String seconds) throws Exception {
		init();
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			jedis.set(key, value);
			if (seconds != null && Integer.valueOf(seconds) > 0) {
				jedis.expire(key, Integer.valueOf(seconds));
			}
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}

	}
	@Override
	public String get(String key) throws Exception {
		init();
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			return jedis.get(key);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}

	}
	@Override
	public String getAndSet(String key, String seconds) throws Exception {
		init();
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			String value = jedis.get(key);
			jedis.select(library);
			jedis.set(key, value);
			if (seconds != null && Integer.valueOf(seconds) > 0) {
				jedis.expire(key, Integer.valueOf(seconds));
			}
			return value;
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}
	@Override
	public String getAndDel(String key) throws Exception {
		init();
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			String value = jedis.get(key);
			jedis.del(key);
			return value;
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

	@Override
	public void del(String... key) throws Exception {
		init();
		Jedis jedis = null;
		try {
			jedis = pool.getResource();
			jedis.select(library);
			jedis.del(key);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

}

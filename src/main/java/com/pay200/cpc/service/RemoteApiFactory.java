package com.pay200.cpc.service;

import microservice.api.ServiceApi;

public interface RemoteApiFactory {

	public  ServiceApi getMsgApi();
}
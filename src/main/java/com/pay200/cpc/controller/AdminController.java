package com.pay200.cpc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class AdminController extends BaseController {

	@RequestMapping(value = "index.html")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("index");
	}

	@RequestMapping(value = "lottery.html")
	public ModelAndView lottery(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("lottery");
	}
}

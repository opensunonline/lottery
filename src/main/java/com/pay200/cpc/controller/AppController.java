package com.pay200.cpc.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.controller.param.AjaxResult;
import com.pay200.cpc.model.Barrage;
import com.pay200.cpc.model.Prize;
import com.pay200.cpc.model.PrizeUser;
import com.pay200.cpc.model.Program;
import com.pay200.cpc.model.SignUser;
import com.pay200.cpc.util.RedisUtil;

/**
 * APP接口
 * @author yangyang.zhang
 * @Package com.pay200.cpc.controller 
 * @Date 2018年1月11日 下午1:58:16 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
@Controller
@RequestMapping("/app/")
public class AppController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(AppController.class);
	
	//获取签到人数
	@RequestMapping(value = "getSignNum.html")
	public void getSignNum(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		List<SignUser> signUsers = SignUser.dao.findAll();
		JSONObject resJson = new JSONObject();
		if(signUsers == null) {
			resJson.put("unm", 0);
		} else {
			resJson.put("num", signUsers.size());
		}
		result.setSucc(true);
		result.setData(resJson);
		result.setMesg("获取成功");
		render(response, result);
	}
	
	//获取签到用户
	@RequestMapping(value = "getSignUser.html")
	public void getSignUser(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		JSONArray array = SignUser.dao.findJsonAll();
		resJson.put("list", array);
		result.setData(resJson);
		result.setMesg("查询成功");
		result.setSucc(true);
		render(response, result);
	}
	//获取可参与抽奖用户
	@RequestMapping(value = "getPUser.html")
	public void getPUser(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		JSONArray array = SignUser.dao.findJson("state", "1");
		resJson.put("list", array);
		result.setData(resJson);
		result.setMesg("查询成功");
		result.setSucc(true);
		render(response, result);
	}
	
	//获取中奖人数
	@RequestMapping(value = "getPrizeUserNum.html")
	public void getPrizeUserNum(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		List<PrizeUser> prizeUsers = PrizeUser.dao.findAll();
		JSONObject resJson = new JSONObject();
		resJson.put("num", prizeUsers.size());
		result.setMesg("查询成功");
		result.setSucc(true);
		result.setData(resJson);
		render(response, result);
	}
	
	//获取中奖用户
	@RequestMapping(value = "getPrizeUser.html")
	public void getPrizeUser(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		JSONArray array = PrizeUser.dao.findJsonAll();
		JSONArray resJsonArr = new JSONArray();
		outer:
		for(int i = 0; i < array.size(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			for(int k = 0; k < resJsonArr.size(); k++) {
				JSONObject object = resJsonArr.getJSONObject(k);
				if(jsonObject.getInteger("prizeId") == object.getInteger("id")) {
					object.put("lotteryNum", object.getInteger("lotteryNum") + 1);
					JSONArray jsonArray = object.getJSONArray("lottery");
					jsonArray.add(jsonObject);
					continue outer;
				}
			}
			JSONObject object = new JSONObject();
			JSONArray array2 = new JSONArray();
			object.put("id", jsonObject.getInteger("prizeId"));
			object.put("lotteryLevel", jsonObject.getString("prizeLevel"));
			object.put("lotteryName", jsonObject.getString("pName"));
			object.put("lotteryNum", 1);
			array2.add(jsonObject);
			object.put("lottery", array2);
			resJsonArr.add(0, object);
		}
		resJson.put("list", resJsonArr);
		result.setData(resJson);
		result.setMesg("查询成功");
		result.setSucc(true);
		render(response, result);
	}
	
	//获取奖品
	@RequestMapping(value = "getPrize.html")
	public void getPrize(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		JSONArray array = Prize.dao.findJsonAll();
		resJson.put("list", array);
		result.setData(resJson);
		result.setMesg("查询成功");
		result.setSucc(true);
		render(response, result);
	}
	
	//获取节目单
	@RequestMapping(value = "getProgram.html")
	public void getProgram(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		JSONArray array = Program.dao.findJsonAll();
		resJson.put("list", array);
		result.setData(resJson);
		result.setMesg("查询成功");
		result.setSucc(true);
		render(response, result);
	}
	
	@RequestMapping(value = "initLottery.html")
	public void initLottery(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		try {
			RedisUtil.getAndDel("prizeUser");
			result.setSucc(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		render(response, result);
	}
	
	//获取剩余抽奖人数
	@RequestMapping(value = "getResNum.html")
	public void getResNum(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		JSONArray signUserArr = SignUser.dao.findJson("state", "1");
		JSONArray prizeUserArr = PrizeUser.dao.findJsonAll();
		JSONArray surePrizeUserArr = new JSONArray();
		outer:
		for(int i = 0; i < signUserArr.size(); i++) {
			JSONObject userJson = signUserArr.getJSONObject(i);
			for(Object object2 : prizeUserArr) {
				JSONObject prizeJson = (JSONObject) object2;
				if(userJson.getString("openId").equals(prizeJson.getString("openId"))) {
					continue outer;
				}
			}
			surePrizeUserArr.add(userJson);
		}
		resJson.put("num", surePrizeUserArr.size());
		result.setData(resJson);
		result.setMesg("查询成功");
		result.setSucc(true);
		render(response, result);
	}
	
	//抽奖逻辑
	@RequestMapping(value = "drawLottery.html")
	public void drawLottery(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String data = request.getParameter("data");
		JSONObject dataJson = JSONObject.parseObject(data);
		String prizeId = dataJson.getString("prizeId");
		String model = dataJson.getString("model");		//抽奖模式
		Prize cPrize = Prize.dao.findFirst("id", prizeId);
		JSONArray signUserArr = SignUser.dao.findJson("state", "1");
		JSONArray prizeUserArr = PrizeUser.dao.findJsonAll();
		JSONArray surePrizeUserArr = new JSONArray();
		//1.取出所有的未抽中奖品的用户,判断抽奖模式（全员，剩余）
		if("All".equals(model)) {
			surePrizeUserArr.addAll(signUserArr);
		} else {
			outer:
			for(int i = 0; i < signUserArr.size(); i++) {
				JSONObject userJson = signUserArr.getJSONObject(i);
				for(Object object2 : prizeUserArr) {
					JSONObject prizeJson = (JSONObject) object2;
					if(userJson.getString("openId").equals(prizeJson.getString("openId"))) {
						continue outer;
					}
				}
				surePrizeUserArr.add(userJson);
			}
		}
		//2.生成用户数量之间的随机数
//		int index = (int) (Math.random() * surePrizeUserArr.size());
		try{
			int index = RandomUtils.nextInt(0, surePrizeUserArr.size());
			JSONObject resJson = surePrizeUserArr.getJSONObject(index);
			//3.将中奖用户添加到中奖名单里
			PrizeUser prizeUser = new PrizeUser();
			prizeUser.put("openId", resJson.getString("openId")).put("headImg", resJson.getString("headImg")).put("nickName", resJson.getString("nickName"))
			.put("prizeLevel", cPrize.getStr("pLevel")).put("pName", cPrize.getStr("pName")).put("prizeId", prizeId).put("createDate", format.format(new Date()));
			//3.减少奖品数量
			if(prizeUser.save()) {
				Prize prize = Prize.dao.findFirst("id", prizeId);
				if(prize != null) {
					String pRes = prize.getStr("pRes");
					Integer count = Integer.parseInt(pRes);
					prize.put("pRes", String.valueOf(count - 1)).update();
				}
			}
			result.setData(resJson);
			result.setMesg("抽奖成功");
			result.setSucc(true);
		}catch(Exception e) {
			logger.info("抽奖失败", e);
			result.setMesg("抽奖失败");
		}
		render(response, result);
	}
	//获取实时弹幕
	@RequestMapping(value = "getBarrage.html")
	public void getBarrage(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		JSONObject resJson = new JSONObject();
		String time = request.getParameter("time");
		JSONArray array = Barrage.dao.findJson("time", time);
		resJson.put("list", array);
		result.setData(resJson);
		result.setMesg("获取成功");
		result.setSucc(true);
		render(response, result);
	}
}

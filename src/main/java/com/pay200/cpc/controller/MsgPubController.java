package com.pay200.cpc.controller;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.model.Barrage;
import com.pay200.cpc.model.SignUser;
import com.pay200.cpc.msg.in.InMsg;
import com.pay200.cpc.msg.in.InNotDefinedMsg;
import com.pay200.cpc.msg.in.InTextMsg;
import com.pay200.cpc.msg.in.event.InNotDefinedEvent;
import com.pay200.cpc.msg.in.event.InQrCodeEvent;
import com.pay200.cpc.msg.out.OutTextMsg;
import com.pay200.cpc.service.RemoteApiFactory;
import com.pay200.cpc.service.WxMsgUtilService;
import com.pay200.cpc.util.URLConnection;
import com.pay200.cpc.util.XMLParse;

import microservice.api.ServiceApiHelper;
import microservice.api.ServiceResult;

/**
 * 微信接收通知接口
 * @author yangyang.zhang
 * @Package com.pay200.cpc.controller 
 * @Date 2018年1月9日 上午10:22:21 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
@Controller
@RequestMapping("/wxmsg/")
public class MsgPubController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(MsgPubController.class);
	private static ResourceBundle bundle = ResourceBundle.getBundle("weixin");

	@Autowired
	private WxMsgUtilService wxMsgUtilService;
	
	@Autowired
	private RemoteApiFactory remoteApiFactory;

	@RequestMapping(value = "wxPub/{appId}.html")
	public void msgBack(HttpServletRequest request, HttpServletResponse response, @PathVariable String appId) {
		if (wxMsgUtilService.isConfigServerRequest(request)) {
			wxMsgUtilService.configServer(request, response);
			return;
		}
		if (!wxMsgUtilService.checkSignature(request)) {
			logger.info("签名验证失败：" + "appId = " + appId);
			return;
		}
		StringBuilder result = new StringBuilder();
		String resultStr = null;
		try {
			InputStream in = request.getInputStream();
	        byte[] b = new byte[4096];  
	        for (int n; (n = in.read(b)) != -1;) {  
	        	result.append(new String(b, 0, n, "UTF-8"));  
	        }
	        logger.info(result.toString());
	        InMsg inMsg = XMLParse.doParse(result.toString());
	        if(inMsg instanceof InTextMsg)
	        	processInTextMsg((InTextMsg) inMsg, response);
	        else if(inMsg instanceof InQrCodeEvent)
	        	processInQrCodeEvent((InQrCodeEvent) inMsg, response);
	        else if(inMsg instanceof InNotDefinedEvent) {
	        	logger.info("未能识别的事件类型。 消息 xml 内容为：\n" + result.toString());
	        	processInNotDefinedEvent((InNotDefinedEvent) inMsg, response);
	        }
	        else if(inMsg instanceof InNotDefinedMsg){
	        	logger.info("未能识别的消息类型。 消息 xml 内容为：\n" + result.toString());
	        	processInNotDefinedMsg((InNotDefinedMsg) inMsg, response);
	        }
		} catch (Exception e) {
			logger.error("公众号通知回调处理异常：appId=[" + appId + "],data=[" + resultStr + "]", e);
		}
	}
	//未能识别的消息类型
	private void processInNotDefinedMsg(InNotDefinedMsg inMsg, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
	}
	//未能识别的事件类型
	private void processInNotDefinedEvent(InNotDefinedEvent inMsg, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
	}
	//扫码时间推送
	private void processInQrCodeEvent(InQrCodeEvent inMsg, HttpServletResponse response) {
		String eventKey = inMsg.getEventKey();
		logger.info("eventKey===" + eventKey);
		if("123".equals(eventKey) || "qrscene_123".equals(eventKey)) {
			OutTextMsg outMsg = new OutTextMsg(inMsg);
			//获取openID，通过openID获取用户信息
			String openID = inMsg.getFromUserName();
			SignUser user = SignUser.dao.findFirst("openId", openID);
			if(user != null) {
				outMsg.setContent("你已签到，点击链接<a href='" + bundle.getString("mainUrl") +"'>进入主会场</a>");
				render(response, outMsg);
				return;
			}
			// TODO 需要accessToken  
			JSONObject object = new JSONObject();
			object.put("appId", bundle.getString("appId"));
			String json = ServiceApiHelper.formatParam("obtainToken", object.toJSONString(), "");
			String resultStr = remoteApiFactory.getMsgApi().execute(json);
			ServiceResult result = ServiceApiHelper.parseResult(resultStr);
			if(result.isSucc()) {
				outMsg.setContent("签到成功,点击下方链接<a href='" + bundle.getString("mainUrl") +"'>进入主会场</a>");
				String data = result.getData();
				JSONObject jsonObject = JSONObject.parseObject(data);
				String accessToken = jsonObject.getString("accessToken");
				String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + accessToken + "&openid=" + openID + "&lang=zh_CN";
				String res = URLConnection.doGet(url, "utf-8");
				if(!StringUtils.isEmpty(res)) {
					JSONObject resJson = JSONObject.parseObject(res);
					String errmsg = resJson.getString("errmsg");
					if(StringUtils.isEmpty(errmsg)) {
						//取到用户信息
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						SignUser signUser = new SignUser();
						signUser.put("nickName", resJson.getString("nickname"));
						signUser.put("headImg", resJson.getString("headimgurl"));
						signUser.put("signTime", format.format(new Date()));
						signUser.put("openId", openID);
						signUser.put("state", "0");
						if(signUser.save()) {
							logger.info("签到成功");
							outMsg.setContent("签到成功,点击下方链接<a href='" + bundle.getString("mainUrl") +"'>进入主会场</a>");
						} else {
							logger.info("签到失败,保存信息失败");
							outMsg.setContent("签到失败，获取不到用户信息");
						}
					} else {
						logger.info("签到失败，获取不到用户信息:" + errmsg + "=========>" + resJson.getString("errcode"));
						outMsg.setContent("签到失败，获取不到用户信息");
					}
				} else {
					logger.info("签到失败，获取不到用户信息: openId=" + openID);
					outMsg.setContent("签到失败，获取不到用户信息");
				}
			} else {
				logger.info("签到失败，获取accessToken失败: resultStr=" + resultStr);
				outMsg.setContent("签到失败，获取不到用户信息");
			}
			render(response, outMsg);
		} else {
			render(response, "success");
		}
	}
	//文本消息推送
	private void processInTextMsg(InTextMsg inMsg, HttpServletResponse response) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SignUser signUser = SignUser.dao.findFirst("openId", inMsg.getFromUserName());
		if(signUser != null) {
			Barrage barrage = new Barrage();
			barrage.put("openId", inMsg.getFromUserName()).put("headImg", signUser.getStr("headImg")).put("content", inMsg.getContent())
			.put("time", format.format(new Date())).save();
		}
		render(response, "success");
	}
	
}

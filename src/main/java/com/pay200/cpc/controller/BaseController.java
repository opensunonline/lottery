package com.pay200.cpc.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.pay200.cpc.controller.param.AjaxResult;
import com.pay200.cpc.msg.out.OutMsg;

public class BaseController {
	
	public void render(HttpServletResponse response, AjaxResult result) {
		write(response, result.toJSONString());
	}
	
	public void render(HttpServletResponse response, OutMsg outMsg) {
		write(response, outMsg.toXml());
	}
	
	public void render(HttpServletResponse response, String text) {
		write(response, text);
	}

	public void write(HttpServletResponse response, String text) {
		try {
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
			response.addHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
			response.addHeader("X-Powered-By","3.2.1");
			response.addHeader("Content-Type", "application/json;charset=utf-8");
			response.getWriter().write(text);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

package com.pay200.cpc.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.controller.param.AjaxResult;
import com.pay200.cpc.model.Prize;
import com.pay200.cpc.model.Program;
/**
 * 节目管理
 */
@Controller
@RequestMapping("/programMg/")
public class ProgramMgController extends BaseController{
	//查询节目单
	@RequestMapping(value="qryProgram.html",method=RequestMethod.GET)
    public ModelAndView qryProgram(HttpServletRequest req, HttpServletResponse resp){
		ModelAndView mv =createMV("programIndex");
		JSONArray resArr = Program.dao.findJsonAll();
		JSONObject obj = new JSONObject();
		obj.put("list",resArr);
		System.out.println("返回数据："+obj);
		mv.addObject("data",obj );
        return mv;
    }
	//查询节目评分
	@RequestMapping(value="qryScore.html",method=RequestMethod.GET)
    public ModelAndView qryScore(HttpServletRequest req, HttpServletResponse resp){
		ModelAndView mv =createMV("scoreIndex");
		JSONArray resArr = Program.dao.findJsonAll();
		JSONObject obj = new JSONObject();
		obj.put("list",resArr);
		System.out.println("返回数据："+obj);
		mv.addObject("data",obj );
        return mv;
    }
	//去添加节目
		@RequestMapping(value = "toAddProgram.html", method = RequestMethod.GET)
		public ModelAndView toAdd(HttpServletRequest req, HttpServletResponse resp) {
			ModelAndView mv = createMV("addProgram");
			return mv;
		}
	//添加节目
	@RequestMapping(value = "addProgram.html", method = RequestMethod.POST)
	public void addProgram(HttpServletRequest req, HttpServletResponse resp) {
		String name = req.getParameter("name");
		String performer = req.getParameter("performer");
		String time = req.getParameter("time");	//表演时间
		String desc = req.getParameter("desc");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(name)) {
			this.write(resp, "节目名称不能为空");
			return;
		}
		if (StringUtils.isEmpty(performer)) {
			this.write(resp, "表演者不能为空");
			return;
		}
		if (StringUtils.isEmpty(time)) {
			this.write(resp, "表演时间不能为空");
			return;
		}
		if (StringUtils.isEmpty(desc)) {
			this.write(resp, "节目描述不能为空");
			return;
		}
		//获取当前时间
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		String createDate = df.format(c.getTime());
		String cScore ="0";
		String pScore ="0";
		String tScore = "0";
		Program Program = new Program();
		boolean status = Program.put("proName",name ).put("performer", performer).put("time", time)
				.put("proDesc", desc).put("ct",createDate).put("cScore", cScore)
				.put("pScore",pScore).put("tScore",tScore).save();
		ajaxResult.setMesg("节目添加成功");
		ajaxResult.setSucc(status);
		render(resp, ajaxResult);
	}
	//添加节目评分
	@RequestMapping(value = "addScore.html", method = RequestMethod.POST)
	public void addScore(HttpServletRequest req, HttpServletResponse resp) {
		String name = req.getParameter("name");
		String performer = req.getParameter("performer");
		String desc = req.getParameter("desc");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(name)) {
			this.write(resp, "节目名称不能为空");
			return;
		}
		if (StringUtils.isEmpty(performer)) {
			this.write(resp, "表演者不能为空");
			return;
		}
		if (StringUtils.isEmpty(desc)) {
			this.write(resp, "节目描述不能为空");
			return;
		}
		//获取当前时间
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		String createDate = df.format(c.getTime());
		Prize prize = new Prize();
		boolean status = prize.put("proName",name ).put("performer", performer).put("pDesc", desc).put("ct",createDate).save();
		ajaxResult.setMesg("节目添加成功");
		ajaxResult.setSucc(status);
		render(resp, ajaxResult);
	}
	//删除节目
	@RequestMapping(value = "delProgram.html", method = RequestMethod.POST)
	public void delProgram(HttpServletRequest req, HttpServletResponse resp) {
		String id = req.getParameter("id");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(id)) {
			this.write(resp, "id不能为空");
			return;
		}
		Program pro = Program.dao.findFirst("id",id );
		pro.delete();
		ajaxResult.setMesg("删除节目成功");
		render(resp, ajaxResult);
	}
	
	private ModelAndView createMV(String jsp) {
		ModelAndView mv = new ModelAndView("program/" + jsp);
		return mv;
	}

	
}

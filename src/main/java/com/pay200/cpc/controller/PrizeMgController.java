package com.pay200.cpc.controller;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.controller.param.AjaxResult;
import com.pay200.cpc.model.Prize;
import com.pay200.cpc.model.PrizeUser;

import microservice.api.ServiceApiHelper;
import microservice.api.ServiceResult;

/**
 * 奖品管理
 */
@Controller
@RequestMapping("/prizeMg/")
public class PrizeMgController extends BaseController{
	//查询奖品
	@RequestMapping(value="qryPrize.html",method=RequestMethod.GET)
    public ModelAndView qryPrize(HttpServletRequest req, HttpServletResponse resp){
		ModelAndView mv =createMV("prizeIndex");
		JSONArray resArr = Prize.dao.findJsonAll();
		JSONObject obj = new JSONObject();
		obj.put("list",resArr);
		System.out.println("返回数据："+obj);
		mv.addObject("data",obj );
        return mv;
    }
	//查询获奖名单
	@RequestMapping(value="qryLucky.html",method=RequestMethod.GET)
    public ModelAndView toLuckyList(HttpServletRequest req, HttpServletResponse resp){
		ModelAndView mv =createMV("luckyIndex");
		JSONArray resArr = PrizeUser.dao.findJsonAll();
		JSONObject obj = new JSONObject();
		obj.put("list",resArr);
		System.out.println("返回数据："+obj);
		mv.addObject("data",obj );
        return mv;
    }
	//去添加
	@RequestMapping(value = "toAddPrize.html", method = RequestMethod.GET)
	public ModelAndView toAdd(HttpServletRequest req, HttpServletResponse resp) {
		ModelAndView mv = createMV("addPrize");
		return mv;
	}
	//添加奖品
	@RequestMapping(value = "addPrize.html", method = RequestMethod.POST)
	public void addPrize(HttpServletRequest req, HttpServletResponse resp) {
		String name = req.getParameter("name");
		String count = req.getParameter("count");
		String level = req.getParameter("level");
		String desc = req.getParameter("desc");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(name)) {
			this.write(resp, "名称不能为空");
			return;
		}
		if (StringUtils.isEmpty(count)) {
			this.write(resp, "总数不能为空");
			return;
		}
		if (StringUtils.isEmpty(level)) {
			this.write(resp, "等级不能为空");
			return;
		}
		if (StringUtils.isEmpty(desc)) {
			this.write(resp, "描述不能为空");
			return;
		}
		//获取当前时间
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		String createDate = df.format(c.getTime());
		Prize prize = new Prize();
		boolean status = prize.put("pName",name ).put("pCount", count).put("pLevel", level).put("pDesc", desc).put("ct",createDate).put("pRes",count).save();
		ajaxResult.setMesg("奖品添加成功");
		ajaxResult.setSucc(status);
		render(resp, ajaxResult);
	}
	//去修改
	@RequestMapping(value="toUpd.html",method=RequestMethod.GET)
    public ModelAndView toUpd(HttpServletRequest req, HttpServletResponse resp) throws Exception{
		ModelAndView mv =createMV("updatePrize");
		String pid = req.getParameter("id");
		String name1 = req.getParameter("name");
		String level1 = req.getParameter("level");
		String count1 = req.getParameter("count");
		String res1 = req.getParameter("res");
		String desc1 = req.getParameter("des");
//		String name = new String(name1.getBytes("iso8859-1"),"UTF-8");
//		String level = new String(level1.getBytes("iso8859-1"),"UTF-8");
//		String count = new String(count1.getBytes("iso8859-1"),"UTF-8");
//		String res = new String(res1.getBytes("iso8859-1"),"UTF-8");
//		String desc = new String(desc1.getBytes("iso8859-1"),"UTF-8");
		mv.addObject("pid",pid);
		mv.addObject("name",name1 );
		mv.addObject("level",level1 );
		mv.addObject("count",count1 );
		mv.addObject("res",res1 );
		mv.addObject("desc",desc1 );
        return mv;
    }
	//修改奖品
	@RequestMapping(value="updPrize.html",method=RequestMethod.POST)
    public void updPrize(HttpServletRequest req, HttpServletResponse resp) throws Exception{
		String id = req.getParameter("id");
		String name = req.getParameter("name");
		String count = req.getParameter("count");
		String level = req.getParameter("level");
		String desc = req.getParameter("desc");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(name)) {
			this.write(resp, "名称不能为空");
			return;
		}
		if (StringUtils.isEmpty(count)) {
			this.write(resp, "总数不能为空");
			return;
		}
		if (StringUtils.isEmpty(level)) {
			this.write(resp, "等级不能为空");
			return;
		}
		if (StringUtils.isEmpty(desc)) {
			this.write(resp, "描述不能为空");
			return;
		}
		//获取当前时间
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		String createDate = df.format(c.getTime());
		Prize prize = Prize.dao.findFirst("id", id);
		boolean stau = prize.update();
		if(stau==true){
			boolean status = prize.put("pName",name ).put("pCount", count).put("pLevel", level).put("pDesc", desc).put("ct",createDate).put("pRes",count).update();
			ajaxResult.setMesg("奖品修改成功");
			ajaxResult.setSucc(status);
		}else{
			ajaxResult.setMesg("奖品修改失败");
		}
		render(resp, ajaxResult);
    }
	//删除奖品
	@RequestMapping(value = "delPrize.html", method = RequestMethod.POST)
	public void delPrize(HttpServletRequest req, HttpServletResponse resp) {
		String id = req.getParameter("id");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(id)) {
			this.write(resp, "id不能为空");
			return;
		}
		Prize prize = Prize.dao.findFirst("id",id );
		prize.delete();
		ajaxResult.setMesg("删除奖品成功");
		render(resp, ajaxResult);
	}
	//获取中的奖品	
	@RequestMapping(value = "qryMinePrize.html", method = RequestMethod.POST)
	public void qryMinePrize(HttpServletRequest req, HttpServletResponse resp) {
		String openId = req.getParameter("openId");
		AjaxResult ajaxResult = new AjaxResult();
		if (StringUtils.isEmpty(openId)) {
			this.write(resp, "获取不到openId");
			return;
		}
		JSONObject prizeUser = PrizeUser.dao.findJsonFirst("openId", openId);
		ajaxResult.setData(prizeUser);
		ajaxResult.setMesg("获取成功");
		ajaxResult.setSucc(true);
		render(resp, ajaxResult);
	}
	
	private ModelAndView createMV(String jsp) {
		ModelAndView mv = new ModelAndView("prize/" + jsp);
		return mv;
	}
}

package com.pay200.cpc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.controller.param.AjaxResult;
import com.pay200.cpc.model.Prize;
import com.pay200.cpc.model.SignUser;
import com.pay200.cpc.msg.out.OutMsg;
/**
 * 签到管理
 */
@Controller
@RequestMapping("/signMg/")
public class SignMgController extends BaseController{
	//查询签到名单
	@RequestMapping(value="qrySign.html",method=RequestMethod.GET)
    public ModelAndView qrySign(HttpServletRequest req, HttpServletResponse resp){
		ModelAndView mv =createMV("signIndex");
		JSONArray resArr = SignUser.dao.findJsonAll();
		JSONObject obj = new JSONObject();
		obj.put("list",resArr);
		System.out.println("返回数据："+obj);
		mv.addObject("data",obj );
        return mv;
    }
	//确认/取消
	@RequestMapping(value="toConfirm.html",method=RequestMethod.POST)
	public void toConfirm(HttpServletRequest req, HttpServletResponse resp){
		AjaxResult ajaxResult = new AjaxResult();
		String id = req.getParameter("id");
		String state = req.getParameter("state");
		SignUser user = SignUser.dao.findFirst("id", id);
		boolean status=false;
		if("1".equals(state)){
			//取消
			 status = user.put("state", "0").update();
		}else{
			//确认
			 status = user.put("state", "1").update();
		}
		if(status==true){
			ajaxResult.setMesg("修改状态成功");
			ajaxResult.setSucc(status);
		}else{
			ajaxResult.setMesg("修改状态失败");
		}
		render(resp, ajaxResult);
	}
	private ModelAndView createMV(String jsp) {
		ModelAndView mv = new ModelAndView("sign/" + jsp);
		return mv;
	}
	
}

package com.pay200.cpc.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.controller.param.AjaxResult;
import com.pay200.cpc.model.Barrage;
import com.pay200.cpc.model.PrizeUser;
import com.pay200.cpc.model.Program;
import com.pay200.cpc.model.SignUser;
import com.pay200.cpc.util.URLConnection;

/**
 * 客户端
 * @author yangyang.zhang
 * @Package com.pay200.cpc.controller 
 * @Date 2018年1月17日 下午5:53:31 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
@Controller
@RequestMapping("/client/")
public class ClientController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(ClientController.class);
	private static ResourceBundle bundle = ResourceBundle.getBundle("weixin");
	private String[] colors = {"#FF4500", "#C0FF3E", "#030303", "#FFFFFF", "#436EEE", "#7A378B", "#C67171"};
	//去主会场
	@RequestMapping(value = "toMeet.html", method = RequestMethod.GET)
	public ModelAndView toMeet(HttpServletRequest req, HttpServletResponse resp) {
		ModelAndView mv = createMV("meetIndex");
		HttpSession session = req.getSession();
		String openId = (String) session.getAttribute("openId");
//		String openId = req.getParameter("openId");
		//判断session是否
		if(StringUtils.isEmpty(openId)) {
			String code = req.getParameter("code");
			logger.info("code", code);
			String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+ bundle.getString("appId") +"&secret="+ bundle.getString("appSecret") +"&code="+ code +"&grant_type=authorization_code";
			String res = URLConnection.doGet(url, "utf-8");
			logger.info(res);
			if(!StringUtils.isEmpty(res)) {
				JSONObject resJson = JSONObject.parseObject(res);
				String errmsg = resJson.getString("errmsg");
				if(StringUtils.isEmpty(errmsg)) {
					//获取用户openId
					session.setAttribute("openId", resJson.getString("openid"));
				}
			}
		}
		return mv;
	}
	
	//发送弹幕
	@RequestMapping(value = "sendBarrage.html")
	public void sendBarrage(HttpServletRequest request, HttpServletResponse response) {
		AjaxResult result = new AjaxResult();
		HttpSession session = request.getSession();
		//发送者openId
		String openId = (String) session.getAttribute("openId");
//		String openId = "okXzjw83uWd5__EUFk0UQr8uqTjA";
		//弹幕信息
		String content = request.getParameter("content");
		if(StringUtils.isEmpty(openId)) {
			result.setMesg("获取用户信息失败，请重新进入页面！");
			render(response, result);
			return;
		}
		if(StringUtils.isEmpty(content)) {
			result.setMesg("请输入弹幕信息");
			render(response, result);
			return;
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SignUser signUser = SignUser.dao.findFirst("openId", openId);
		if(signUser != null) {
			Barrage barrage = new Barrage();
			barrage.put("openId", openId).put("headImg", signUser.getStr("headImg")).put("content", content).put("color", colors[RandomUtils.nextInt(0, colors.length)])
			.put("time", format.format(new Date())).save();
		} else {
			result.setMesg("请先签到再进行操作");
			render(response, result);
			return;
		}
		result.setMesg("发送成功");
		result.setSucc(true);
		render(response, result);
	}
		
	//去节目列表
	@RequestMapping(value = "toPro.html", method = RequestMethod.GET)
	public ModelAndView toPro(HttpServletRequest req, HttpServletResponse resp) {
		ModelAndView mv = createMV("proIndex");
		JSONArray resArr = Program.dao.findJsonAll();
		JSONObject obj = new JSONObject();
		obj.put("list", resArr);
		mv.addObject("data", obj);
		return mv;
	}

	//去奖品列表
	@RequestMapping(value = "toPrize.html", method = RequestMethod.GET)
	public ModelAndView toPrize(HttpServletRequest req, HttpServletResponse resp) {
		ModelAndView mv = createMV("ownerIndex");
		HttpSession session = req.getSession();
		String openId = (String) session.getAttribute("openId");
		if(StringUtils.isEmpty(openId)) {
			openId = "";
		}
		JSONObject prizeUser = PrizeUser.dao.findJsonFirst("openId", openId);
		if(prizeUser != null) {
			String prizeName = prizeUser.getString("pName");
			mv.addObject("prizeName", prizeName);
		} else {
			mv.addObject("prizeName", "未中奖");
		}
		return mv;
	}
	
	private ModelAndView createMV(String jsp) {
		ModelAndView mv = new ModelAndView("client/" + jsp);
		return mv;
	}
	
}

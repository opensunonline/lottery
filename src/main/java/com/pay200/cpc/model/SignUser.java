package com.pay200.cpc.model;
/**
 * 签到用户
 * @author yangyang.zhang
 * @Package com.pay200.cpc.model 
 * @Date 2018年1月10日 下午5:41:56 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
public class SignUser extends Model<SignUser> {
	
	private static final long serialVersionUID = 7496178060655389651L;
	public static final SignUser dao = new SignUser();
}

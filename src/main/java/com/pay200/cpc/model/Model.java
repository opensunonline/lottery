package com.pay200.cpc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pay200.cpc.util.RedisUtil;

public abstract class Model<M extends Model> implements Serializable {
	
	private static final long serialVersionUID = 5253617708952553457L;
	
	private Map<String, Object> attrs = getAttrsMap();
	
	private Map<String, Object> getAttrsMap() {
		Map<String, Object> map = new HashMap<>();
		map.clear();
		return map;
	}
	/**
	 * 插入列
	 * @param attr
	 * @param value
	 * @return
	 */
	public M put(String attr, Object value) {
		attrs.put(attr, value);
		return (M) this;
	}
	
	public String getStr(String attr) {
		return (String)attrs.get(attr);
	}
	
	/**
	 * 保存方法
	 * @return
	 * @throws Exception 
	 */
	public boolean save(){
		//获取表的名字
		String tableName = TableConfig.map.get(this.getClass());
		String data;
		int id = 1;
		try {
			data = RedisUtil.get(tableName);
			JSONObject saveObject = null;
			JSONArray dataArray = null;
			if(StringUtils.isEmpty(data)) {
				saveObject = new JSONObject();
				saveObject.put("index", id);
				dataArray = new JSONArray();
			} else {
				saveObject = JSONObject.parseObject(data);
				id = saveObject.getInteger("index") + 1;
				saveObject.put("index", id);
				dataArray = saveObject.getJSONArray("data");
			}
			JSONObject saveJson = fromObject(attrs);
			saveJson.put("id", id);
			dataArray.add(saveJson);
			saveObject.put("data", dataArray);
			RedisUtil.set(tableName, saveObject.toJSONString(), null);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean update() {
		//获取表的名字
		String tableName = TableConfig.map.get(this.getClass());
		String data;
		try {
			JSONObject saveObject = null;
			JSONArray dataArray = null;
			data = RedisUtil.get(tableName);
			if(StringUtils.isEmpty(data)) {
				return false;
			} else {
				saveObject = JSONObject.parseObject(data);
				dataArray = saveObject.getJSONArray("data");
				for(int i = 0; i < dataArray.size(); i++) {
					JSONObject jsonObject = dataArray.getJSONObject(i);
					if(jsonObject.getInteger("id") == attrs.get("id")) {
						dataArray.remove(i);
						dataArray.add(i, fromObject(attrs));
						break;
					}
				}
				saveObject.put("data", dataArray);
				RedisUtil.set(tableName, saveObject.toJSONString(), null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean delete() {
		String tableName = TableConfig.map.get(this.getClass());
		String data;
		try {
			JSONObject saveObject = null;
			JSONArray dataArray = null;
			data = RedisUtil.get(tableName);
			if(StringUtils.isEmpty(data)) {
				return false;
			} else {
				saveObject = JSONObject.parseObject(data);
				dataArray = saveObject.getJSONArray("data");
				for(int i = 0; i < dataArray.size(); i++) {
					JSONObject jsonObject = dataArray.getJSONObject(i);
					if(jsonObject.getInteger("id") == attrs.get("id")) {
						dataArray.remove(i);
						break;
					}
				}
				saveObject.put("data", dataArray);
				RedisUtil.set(tableName, saveObject.toJSONString(), null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	/**
	 * 查询单条件
	 * @param attr
	 * @param value
	 * @return
	 */
	public List<M> find(String attr, String value) {
		return find(new String[]{attr}, value);
	}
	
	public JSONArray findJson(String attr, String value) {
		return findJson(new String[]{attr}, value);
	}
	
	public List<M> find(String[] attrs, String ...str) {
		//获取表的名字
		try {
			JSONArray array = findJson(attrs, str);
			List<M> list = new ArrayList<>();
			for(Object object : array) {
				JSONObject jsonObject = (JSONObject) object;
				Set<String> keys = jsonObject.keySet();
				M m = (M) this.getClass().newInstance();
				for(String key : keys) {
					m.put(key, jsonObject.get(key));
				}
				list.add(m);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public JSONArray findJson(String[] attrs, String ...str) {
		String tableName = TableConfig.map.get(this.getClass());
		try {
			String data = RedisUtil.get(tableName);
			if(StringUtils.isEmpty(data)) {
				return new JSONArray();
			}
			JSONObject dataObject = JSONObject.parseObject(data);
			JSONArray array = dataObject.getJSONArray("data");
			JSONArray resArray = new JSONArray();
			outer:
			for(Object object : array) {
				JSONObject jsonObject = (JSONObject) object;
				for(int i = 0; i < attrs.length; i++) {
					if(!str[i].equals(jsonObject.getString(attrs[i]))) {
						continue outer;
					}
				}
				resArray.add(jsonObject);
			}
			return resArray;
		} catch (Exception e) {
			e.printStackTrace();
			return new JSONArray();
		}
	}
	
	public List<M> findAll() {
		//获取表的名字
		try {
			JSONArray array = findJsonAll();
			List<M> list = new ArrayList<>();
			for(Object object : array) {
				JSONObject jsonObject = (JSONObject) object;
				Set<String> keys = jsonObject.keySet();
				M m = (M) this.getClass().newInstance();
				for(String key : keys) {
					m.put(key, jsonObject.get(key));
				}
				list.add(m);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	
	public JSONArray findJsonAll() {
		//获取表的名字
		String tableName = TableConfig.map.get(this.getClass());
		try {
			String data = RedisUtil.get(tableName);
			if(StringUtils.isEmpty(data)) {
				return new JSONArray();
			}
			JSONObject dataObject = JSONObject.parseObject(data);
			JSONArray array = dataObject.getJSONArray("data");
			return array;
		} catch (Exception e) {
			e.printStackTrace();
			return new JSONArray();
		}
	}
	
	public JSONObject findJsonFirst(String[] attrs, String ...str) {
		String tableName = TableConfig.map.get(this.getClass());
		try {
			String data = RedisUtil.get(tableName);
			if(StringUtils.isEmpty(data)) {
				return null;
			}
			JSONObject dataObject = JSONObject.parseObject(data);
			JSONArray array = dataObject.getJSONArray("data");
			outer:
			for(Object object : array) {
				JSONObject jsonObject = (JSONObject) object;
				for(int i = 0; i < attrs.length; i++) {
					if(!str[i].equals(jsonObject.getString(attrs[i]))) {
						continue outer;
					}
				}
				return jsonObject;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public M findFirst(String[] attrs, String ...str) {
		JSONObject jsonObject = findJsonFirst(attrs, str);
		if(jsonObject == null)
			return null;
		Set<String> keys = jsonObject.keySet();
		M m = null;
		try {
			m = (M) this.getClass().newInstance();
			for(String key : keys) {
				m.put(key, jsonObject.get(key));
			}
			return m;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public JSONObject findJsonFirst(String attr, String value) {
		return findJsonFirst(new String[]{attr}, value);
	}
	
	public M findFirst(String attr, String value) {
		return findFirst(new String[]{attr}, value);
	}
	
	public JSONObject fromObject(Map<String, Object> map) {
		JSONObject object = new JSONObject();
		for(String key : map.keySet()) {
			object.put(key, map.get(key));
		}
		return object;
	}
}
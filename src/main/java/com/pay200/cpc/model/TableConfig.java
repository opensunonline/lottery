package com.pay200.cpc.model;

import java.util.HashMap;
import java.util.Map;

public class TableConfig {

	public static Map<Class, String> map = new HashMap<>();
			
	static {
		map.put(Prize.class, "prize");		//奖品
		map.put(PrizeUser.class, "prizeUser");		//中奖用户
		map.put(SignUser.class, "signUser");		//签到用户
		map.put(Program.class, "Program");			//节目单
		map.put(Barrage.class, "Barrage");			//弹幕
	}
}

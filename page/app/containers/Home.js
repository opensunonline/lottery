import React from 'react';
import SignWall from "../components/SignWall";
import SignNum from "../components/SignNum";
import Lottery from "../components/lottery/Lottery";
import Program from "../components/program/Program";
import ActivityCode from "../components/ActivityCode";
export default class Home extends React.Component {

    static defaultProps={
        signWallShow: 'block',
    }

    constructor(props, context){
        super(props);
        this.state = {
            signWallShow: 'block',
            lotteryShow: 'none',
            programShow: 'none',
            activityCodeShow: 'none',
            titleShow: 'none',
            tit: ''
        }
    }

    switchType = (type) => {
        let sta = {signWallShow: 'none', lotteryShow: 'none', programShow: 'none', activityCodeShow: 'none'};
        sta[type] = 'block';
        this.setState(sta)
    }

    openCode = () => {
        let sta = this.state
        sta['activityCodeShow'] = 'block'
        this.setState(sta)
    }

    showTitle = (tit) => {
        let sta = this.state
        sta['tit'] = tit
        sta['titleShow'] = ''
        this.setState(sta)
        setTimeout(function () {
            let sta = this.state
            sta['titleShow'] = 'none'
            this.setState(sta)
        }.bind(this), 2000)
    }

    render() {
        const {signWallShow, lotteryShow, programShow, activityCodeShow, titleShow, tit} = this.state;
        return(
            <div>
                <div id="main" style={{width: '1010px'}}>
                    <img src="images/logo.png" id="logo"/>
                    {/*<ActivityCode show={activityCodeShow}/>*/}
                    <SignWall show={signWallShow}/>
                    <Lottery show={lotteryShow} showClick={this.showTitle}/>
                    <Program show={programShow}/>
                    <SignNum/>
                    <div id="btnBox">
                        <ul id="btn_change" style={{display: 'block'}}>
                            <li className="btn_messagewall" data-name="messagewall" data-description="消息墙" onClick={this.switchType.bind(this, 'signWallShow')}><i className="iconfont"></i></li>
                            <li className="btn_lottery" data-name="lottery" data-description="滚动抽奖" onClick={this.switchType.bind(this, 'lotteryShow')}><i className="iconfont"></i></li>
                            {/*<li className="btn_program" data-name="program" data-description="节目评分" onClick={this.switchType.bind(this, 'programShow')}><i className="iconfont">*/}
                                {/*</i>*/}
                            {/*</li>*/}
                            <li className="btn_messageWall" data-name="messagewalldanmu" data-description="弹幕"><i className="iconfont"></i></li>
                            {/*<li className="btn_qrcode" data-name="qrcode" data-description="二维码"><i className="iconfont" onClick={this.openCode}></i></li>*/}
                        </ul>
                    </div>
                </div>
                <div className="info-box" style={{display: titleShow}}>
                    <div className="error">{tit}</div>
                </div>
            </div>
        )
    }
}
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch, HashRouter as Router } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import Home from "../containers/Home";
import Program from "../components/program/Program";
const history = createHistory();
class IndexController extends React.Component {

    static defaultProps={

    }

    constructor(props, context){
        super(props);
    }

    render() {
        return(
            <Router history={history}>
                <Switch>
                    <Route exact path="/" component={Home}/>
                </Switch>
            </Router>);
    }

}
ReactDOM.render(<IndexController/>, document.getElementById('root'))
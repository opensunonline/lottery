import React from 'react';
import ProgramScoreBox from "./ProgramScoreBox";
import AsynPost from '../../common/AsynPost.js'
export default class ProgramListBox extends React.Component {

    static defaultProps = {
        show: 'none'
    }

    constructor(props, context) {
        super(props)
        this.state = {
            data: [
            ],
            scoreBoxShow: 'none'
        }
    }
    /*获取所有的节目*/
    componentWillMount() {
        let data = {}
        AsynPost(data, 'getProgram.html', this.obProgram)
    }

    obProgram = (res) => {
        let sta = this.state
        sta['data'] = res.data.list
        this.setState(sta)
    }

    getItem = () => {
        const itemArr = []
        let sta = this.state
        let data = sta.data
        for(let i = 0; i < data.length; i++) {
            let dataItem = data[i]
            let item = (<li key={i}>
                            <span className="index">
                                <span className="index-num">{i+1}</span>
                            </span>
                            <span className="icon" style={{backgroundImage: 'url(http://file.kxdpm.com/activity_program_thumbnail/201706/0fc27830-8a5c-495b-be6f-59f204aa7354.png)'}}></span>
                            <p className="list-title">{dataItem.proName}</p>
                            <p className="list-text">表演者：{dataItem.performer}</p>
                        </li>)
            itemArr.push(item)
        }
        return itemArr
    }

    openScoreBox = () => {
        let sta = this.state
        sta['scoreBoxShow'] = 'display'
        this.setState(sta)
    }

    render() {
        let {show} = this.props;
        let {scoreBoxShow} = this.state
        let item = this.getItem()
        return(
            <div id="programListBox" style={{display: show}}>
                <div className="title"><i className="icon icon-title"></i>参与评分节目单</div>
                <div className="title-btn">
                    <a className="clickBtn middleBtn" id="showProgramScore" style={{marginRight: '10px'}} onClick={this.openScoreBox}>查看结果</a>
                    <a className="clickBtn middleBtn pushProgram">开始评分</a>
                </div>
                <ul id="programList">
                    {item}
                </ul>
            </div>
        )
    }
}
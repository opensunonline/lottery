import React from 'react';
import ProgramScore from "./ProgramScore";
import ProgramScoreBox from "./ProgramScoreBox";
import ProgramDetail from "./ProgramDetail";
import ProgramListBox from "./ProgramListBox";
export default class Program extends React.Component {

    static defaultProps = {
        show: 'none'
    }

    constructor(props, context) {
        super(props)
        this.state = {
            programListBoxShow: 'block',
            programDetailShow: 'none',
            programScoreBox: 'none'
        }
    }

    render() {
        let {show} = this.props;
        let {programListBoxShow, programDetailShow, programScoreBox} = this.state;
        return(
            <div style={{display: show}} data-moduleid="100110" data-modulename="program" className="module" id="program">
                <ul className="moduleBtn">
                    <li className="btn_program" data-name="program" data-description="节目评分"><i className="iconfont">
                        &#xe64f;</i>
                    </li>
                </ul>
                <div className="program-ing" style={{display: 'block'}}>
                    <ProgramListBox show={programListBoxShow}/>
                    <ProgramDetail show={programDetailShow}/>
                    <ProgramScoreBox show={programScoreBox}/>
                </div>
            </div>
        )
    }
}
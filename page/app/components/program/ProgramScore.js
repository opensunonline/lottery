import React from 'react';
export default class ProgramScore extends React.Component {

    static defaultProps = {

    }

    constructor(props, context) {
        super(props)
    }

    render() {
        return(
            <div style={{position: 'relative'}}>
                <ul id="programScore" style={{width: '640px', left: '50%', marginLeft: '-320px'}}>
                    <li className="top3" data-programid="3864" data-useexpertscore="false" style={{left: '330px'}}>
                        <div className="wrap">
                            <div className="middle-score" style={{display: 'none'}}><span className="total-score">2</span></div>
                            <div className="middle" style={{height: '74.4444px'}}><span className="total-score">2</span></div>
                            <div className="bottom"></div>
                        </div>
                        <h1>这个地方为节目名称的设置</h1></li>
                    <li className="top1" data-programid="3865" data-useexpertscore="false" style={{left: '110px'}}>
                        <div className="wrap">
                            <div className="middle-score" style={{display: 'none'}}><span className="total-score">5</span></div>
                            <div className="middle" style={{height: '186.111px'}}><span className="total-score">5</span></div>
                            <div className="bottom"></div>
                        </div>
                        <h1>此处填写节目名称</h1></li>
                    <li className="top0" data-programid="3866" data-useexpertscore="false" style={{left: '0px'}}>
                        <div className="wrap">
                            <div className="middle-score" style={{display: 'none'}}><span className="total-score">9</span></div>
                            <div className="middle" style={{height: '335px'}}><span className="total-score">9</span></div>
                            <div className="bottom"></div>
                        </div>
                        <h1>评分节目3</h1></li>
                    <li className="top2" data-programid="3867" data-useexpertscore="false" style={{left: '220px'}}>
                        <div className="wrap">
                            <div className="middle-score" style={{display: 'none'}}><span className="total-score">5</span></div>
                            <div className="middle" style={{height: '186.111px'}}><span className="total-score">5</span></div>
                            <div className="bottom"></div>
                        </div>
                        <h1>评分节目4</h1></li>
                </ul>
                <div className="bg-bottom"></div>
            </div>
        )
    }
}
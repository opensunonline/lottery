import React from 'react';
export default class ProgramDetail extends React.Component {

    static defaultProps = {

    }

    constructor(props, context) {
        super(props)
    }

    render() {
        return(
            <div id="programDetail" style={{display: 'block'}}>
                <a className="arrow-left" href="javascript:void(0);"><i className="iconfont">
                    &#xe62e;</i></a>
                <a className="arrow-right" href="javascript:void(0);"><i className="iconfont">
                    &#xe602;</i></a>
                <div className="title"><i className="icon icon-title"></i>节目详情
                    <a className="close"></a>
                </div>
                <ul>

                </ul>
            </div>
        )
    }
}
import React from 'react';
import ProgramScore from "./ProgramScore";
export default class ProgramScoreBox extends React.Component {

    static defaultProps = {
        show: 'none'
    }

    constructor(props, context) {
        super(props)
    }

    render() {
        let {show} = this.props;
        return(
            <div id="programScoreBox" style={{display: show}}>
                <a id="program_socre_close" className="close"></a>

                <div className="title"><i className="icon icon-title"></i>节目分值统计</div>
                <div className="title-btn">
                    <a className="clickBtn middleBtn stopBtn endProgram">关闭评分</a>
                    <a className="clickBtn middleBtn pushProgram">开始评分</a>
                </div>
                <ProgramScore/>
            </div>
        )
    }
}
import React from 'react';
export default class LotteryLuck extends React.Component {

    static defaultProps = {
        lotteryName: '',
        lotteryNum: 0,
        lotteryLevel: '',
        data:[
            {headImg: 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM5xuDWUWxvnW6gCibuFE4Iib5UccPQpsqg9ia1cZ35kEVfBkyvugDaE7gK4JcVIxfANCEJXBRAdEM9Lc4iaIbylnkicx92bib0wOtryQ/0', nickName: 'OpenSuns'}
        ]
    }

    constructor(props, context) {
        super(props)
    }

    getItem = () => {
        const itemArr = []
        let data = this.props.data
        for(let i = 0; i < data.length; i++) {
            let dataItem = data[i]
            let item = (<li key={i} className="submited" data-hasluck="1" data-level={dataItem.id}><img src={dataItem.headImg}/><span>{dataItem.nickName}</span></li>)
            itemArr.push(item)
        }
        return itemArr
    }

    render() {
        let {lotteryName, lotteryNum, lotteryLevel} = this.props
        let item = this.getItem();
        return(
            <div id="level_18795" className="level"><label>{lotteryLevel}{`(`+lotteryName+`)`}：<a>{lotteryNum}</a></label>
                <ul>
                    {item}
                </ul>
            </div>
        )
    }
}
import React from 'react';
import AsynPost from '../../common/AsynPost.js'
export default class LotteryLuckNum extends React.Component {

    static defaultProps = {

    }

    constructor(props, context) {
        super(props)
        this.state = {
            prizeNum: 0
        }
    }

    /*获取中中奖总人数*/
    componentWillMount() {
        setInterval(function () {
            let data = {}
            AsynPost(data, 'getPrizeUserNum.html', this.obPrizeUserNum)
        }.bind(this), 2000)
    }

    obPrizeUserNum = (res) => {
        let sta = this.state
        sta['prizeNum'] = res.data.num
        this.setState(sta)
    }

    render() {
        let {prizeNum} = this.state
        return(
            <div className="resultNum">获奖人数:<span id="luckNumber">{prizeNum}</span></div>
        )
    }
}
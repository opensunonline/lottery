import React from 'react';
import LotteryLuckUL from "./LotteryLuckUL";
import LotteryLuckNum from "./LotteryLuckNum";
import AsynPost from '../../common/AsynPost.js'
import LotteryAnimate from "./LotteryAnimate";
export default class Lottery extends React.Component {

    static defaultProps = {
        show: 'none',
        showClick: undefined
    }

    constructor(props, context) {
        super(props)
        this.state = {
            data: [],
            prizes: [],
            luck_left: 0,
            prizeShow: 'none',
            prizeId: 0,
            prizeName: '请选择',
            prizeNum: 0,
            amountShow: 'none',
            amountName: '请选择',
            amountNum: 0,
            amountResNum: 0,
            resNum: 0,                      /*剩余抽奖人数*/
            modelShow: 'none',            /*抽奖模式显示*/
            model: 'Surplus',             /*抽奖模式（默认为抽取剩余人数）*/
            modelName: '剩余人',          /*默认从剩余人中抽取*/
            rightClassName: 'right',      /*右侧中奖名单class样式*/
            leftShow: 'block',            /*左侧抽奖显示隐藏*/
            animateShow: 'none',
            user_width: 0,                  /*用户宽度*/
            interTime: 20,

            inter: undefined,               /*抽奖进程*/
            startShow: '',                  /*开始抽奖按钮显示*/
            stopShow: 'none',
            autoShow: 'none',


            isStop: false,
            stop_left: 0
        }
    }

    componentWillMount() {
        let data = {}
        setInterval(function () {
            AsynPost(data, 'getPUser.html', this.obLotteryUser)
            AsynPost(data, 'getPrize.html', this.obPrizes)
            AsynPost(data, 'getResNum.html', this.obResNum)
        }.bind(this), 2000)
        setInterval(function () {
            let amount = 0
            let prizes = this.state.prizes
            for(let i = 0; i < prizes.length; i++) {
                let prizeItem = prizes[i]
                if(prizeItem.id == this.state.prizeId) {
                    amount = prizeItem.pRes
                }
            }
            let sta = this.state
            sta['prizeNum'] = amount
            this.setState(sta)
        }.bind(this), 2000)
    }

    obLotteryUser = (res) => {
        console.log(res)
        let sta = this.state
        sta['data'] = res.data.list
        sta['user_width'] = res.data.list.length * 2 * 190
        this.setState(sta)
    }

    obPrizes = (res) => {
        let sta = this.state
        sta['prizes'] = res.data.list
        this.setState(sta)
    }
    /*获取剩余抽奖人数*/
    obResNum = (res) => {
        let sta = this.state
        sta['resNum'] = res.data.num
        this.setState(sta)
    }

    /*打开选择抽奖奖品*/
    showPrize = () => {
        let sta = this.state
        let prizeShow = sta.prizeShow
        if(prizeShow == 'none') {
            sta['prizeShow'] = 'block'
        } else {
            sta['prizeShow'] = 'none'
        }
        this.setState(sta)
    }
    /*选择奖品*/
    selectPrize = (id, name, num) => {
        let sta = this.state
        sta['prizeId'] = id
        sta['prizeName'] = name
        sta['prizeShow'] = 'none'
        sta['prizeNum'] = num
        sta['amountName'] = '请选择'
        sta['amountShow'] = 'none'
        sta['amountNum'] = 0
        this.setState(sta)
    }
    /*生成抽奖人数*/
    genAmount = () => {
        let sta = this.state
        let num = sta.prizeNum
        const itemArr = []
        for(let i = 0; i < num; i++) {
            let item = (<li key={i} data-amount={i+1} onClick={this.seletAmount.bind(this, i + 1)}>{i+1}</li>)
            itemArr.push(item)
        }
        return itemArr
    }
    /*打开选择抽奖人数*/
    showAmount = () => {
        let sta = this.state
        let amountShow = sta.amountShow
        if(amountShow == 'none') {
            sta['amountShow'] = 'block'
        } else {
            sta['amountShow'] = 'none'
        }
        this.setState(sta)
    }
    /*选择抽奖人数*/
    seletAmount = (num) => {
        let sta = this.state
        sta['amountName'] = num
        sta['amountNum'] = num
        sta['amountShow'] = 'none'
        sta['prizeShow'] = 'none'
        this.setState(sta)
    }
    /*展开/关闭中奖名单*/
    openWinning = () => {
        let sta = this.state
        let leftShow = sta.leftShow
        if(leftShow == 'none') {
            sta['leftShow'] = 'block'
            sta['rightClassName'] = 'right'
        } else {
            sta['leftShow'] = 'none'
            sta['rightClassName'] = 'right right-fullscreen'
        }
        this.setState(sta)
    }
    /*展开抽奖模式*/
    showModel = () => {
        let sta = this.state
        let modelShow = sta.modelShow
        if(modelShow == 'none') {
            sta['modelShow'] = 'block'
        } else {
            sta['modelShow'] = 'none'
        }
        this.setState(sta)
    }

    seletModel = (model) => {
        let sta = this.state
        sta['model'] = model
        if(model == 'All') {
            sta['modelName'] = '全员'
        } else {
            sta['modelName'] = '剩余人'
        }
        sta['modelShow'] = 'none'
        this.setState(sta)
    }

    /*生成抽奖用户*/
    getItem = () => {
        const itemArr = []
        let sta = this.state
        let data = sta.data
        for(let i = 0; i < data.length; i++) {
            let dataItem = data[i]
            let item = (<li key={i} data-userid={dataItem.id}><img src={dataItem.headImg}/><br/><span>{dataItem.nickName}</span></li>)
            itemArr.push(item)
        }
        for(let i = 0; i < data.length; i++) {
            let dataItem = data[i]
            let item = (<li key={'t'+i} data-userid={dataItem.id}><img src={dataItem.headImg}/><br/><span>{dataItem.nickName}</span></li>)
            itemArr.push(item)
        }
        return itemArr
    }
    /*生成抽奖奖品*/
    genPrize = () => {
        const itemArr = []
        let prizes = this.state.prizes
        for(let i = 0; i < prizes.length; i++) {
            let dataItem = prizes[i]
            let item = (<li key={i} data-prizeid={dataItem.id} onClick={this.selectPrize.bind(this, dataItem.id, dataItem.pLevel, dataItem.pRes)}>{dataItem.pLevel+`(`+dataItem.pName+`)`} <span>剩<label>{dataItem.pRes}</label>名</span></li>)
            itemArr.push(item)
        }
        return itemArr
    }
    /*获奖动画*/
    startAnimate = () => {
        let sta3 = this.state
        sta3['animateShow'] = 'block'
        this.setState(sta3)
        setTimeout(function () {
            let sta3 = this.state
            sta3['animateShow'] = 'none'
            this.setState(sta3)
        }.bind(this), 3000)
    }
    /*抽奖动画*/
    lotteryAnimate = () => {
        let sta = this.state
        let {luck_left} = this.state
        let l = luck_left - 190
        let lll = this.state.user_width / 2
        if(sta.isStop && sta.stop_left == luck_left - 190) {
            clearInterval(sta.inter)
            this.startAnimate()
        } else {
            if(l <= - lll) {
                sta['luck_left'] = 0
            } else {
                sta['luck_left'] = l
            }
        }
        this.setState(sta)
    }

    /*开启抽奖*/
    startLottery = () => {
        if(this.state.prizeName == "请选择") {
            this.props.showClick('请选择抽奖奖项')
            return
        }
        //判断奖品剩余
        if(this.state.amountNum <= 0) {
            this.props.showClick('请选择抽奖人数')
            return
        }
        //判断剩余奖品剩余数量
        let amount = 0
        let prizes = this.state.prizes
        for(let i = 0; i < prizes.length; i++) {
            let prizeItem = prizes[i]
            if(prizeItem.id == this.state.prizeId) {
                amount = prizeItem.pRes
            }
        }
        if(this.state.amountNum > amount) {
            this.props.showClick('奖品剩余数量不足')
            return
        }
        //判断抽奖人数，判断抽奖模式是否是全员
        if(this.state.model != 'All') {
            if(this.state.resNum < this.state.amountNum) {
                this.props.showClick('剩余抽奖人数不足，如需继续抽奖请选择全员模式')
                return
            }
        }
        /*修改按钮显示*/
        if(this.state.amountNum > 1) {
            /*自动抽奖，点击停止之后再启动自动抽奖*/
            let sta = this.state
            sta['amountResNum'] = sta.amountNum
            sta['typ'] = 'batch'
            this.setState(sta)
            this.startAutoLottery()
            this.btnShow('stopShow')
        } else {
            /*启动抽奖*/
            const inter = setInterval(this.lotteryAnimate.bind(this), 50)
            this.btnShow('stopShow')
            let sta = this.state
            /*存储定时器ID*/
            sta['inter'] = inter
            sta['isStop'] = false
            sta['typ'] = 'single'
            this.setState(sta)
        }
    }
    /*停止抽奖请求*/
    stopLottery = () => {
        /*判断是单个抽奖还是批量*/
        if(this.state.typ == 'single') {
            let data = {'prizeId': this.state.prizeId, 'model': this.state.model}
            AsynPost(data, 'drawLottery.html', this.stopLotteryPost)
        } else {
            this.stopAutoStartLottery()
        }

    }
    /*停止抽奖逻辑*/
    stopLotteryPost = (res) => {
        /*判断抽奖是否成功*/
        let sta = this.state
        if(res.succ) {
            let dataItem = res.data
            let id = dataItem.id
            let data = sta.data
            let index = 0
            for(let i = 0; i < data.length; i++) {
                let dataItem = data[i]
                if(dataItem.id == id) {
                    index = i
                    break;
                }
            }
            let stop_left
            if(index == 0) {
                stop_left = -(sta.user_width / 2)
            } else {
                stop_left = index * -190
            }
            sta['stop_left'] = stop_left
            sta['isStop'] = true
            sta['lotteryUserName'] = data[index].nickName
            sta['lotteryName'] = sta.prizeName
            sta['lotteryHeadImg'] = data[index].headImg
            sta['startShow'] = ''
            sta['stopShow'] = 'none'
            sta['autoShow'] = 'none'
            this.setState(sta)
        } else {
            this.props.showClick('抽奖出现问题')
            this.btnShow('startShow')
            clearInterval(this.state.inter)
        }
    }

    /*自动抽奖动画*/
    lotteryAutoAnimate = () => {
        let sta = this.state
        let {luck_left} = this.state
        let l = luck_left - 190
        let lll = this.state.user_width / 2
        if(sta.isStop && sta.stop_left == luck_left - 190) {
            clearInterval(sta.inter)
            this.startAnimate()
            if(sta.amountResNum > 0) {
                setTimeout(function () {
                    this.startAutoLottery()
                    this.autoLottery()
                }.bind(this), 1000)
            } else {
                this.btnShow('startShow')
            }
        } else {
            if(l <= - lll) {
                sta['luck_left'] = 0
            } else {
                sta['luck_left'] = l
            }
            this.setState(sta)
        }
    }

    /*停止自动抽奖*/
    stopAutoLottery = () => {
        let data = {'prizeId': this.state.prizeId, 'model': this.state.model}
        AsynPost(data, 'drawLottery.html', this.stopAutoLotteryPost)
    }

    /*停止自动抽奖逻辑*/
    stopAutoLotteryPost = (res) => {
        let sta = this.state
        if(res.succ) {
            let dataItem = res.data
            let id = dataItem.id
            let data = sta.data
            let index = 0
            for(let i = 0; i < data.length; i++) {
                let dataItem = data[i]
                if(dataItem.id == id) {
                    index = i
                    break;
                }
            }
            let stop_left
            if(index == 0) {
                stop_left = -(sta.user_width / 2)
            } else {
                stop_left = index * -190
            }
            sta['stop_left'] = stop_left
            sta['isStop'] = true
            sta['lotteryUserName'] = data[index].nickName
            sta['lotteryName'] = sta.prizeName
            sta['lotteryHeadImg'] = data[index].headImg
            sta['amountResNum'] = (sta.amountResNum - 1)
            this.setState(sta)
        } else {
            this.btnShow('startShow')
            clearInterval(this.state.inter)
        }
    }

    /*启动自动抽奖*/
    startAutoLottery = () => {
        /*启动抽奖*/
        let inter = setInterval(this.lotteryAutoAnimate.bind(this), 50)
        let sta2 = this.state
        /*存储定时器ID*/
        sta2['inter'] = inter
        sta2['isStop'] = false
        this.setState(sta2)
    }

    stopAutoStartLottery = () => {
        this.btnShow('autoShow')
        setTimeout(this.stopAutoLottery.bind(this), 100)
        // this.autoLottery()
    }

    /*自动抽奖触发停止*/
    autoLottery = () => {
        setTimeout(this.stopAutoLottery.bind(this), 2000)
    }

    btnShow = (dis) => {
        let sta = this.state
        if(dis == 'startShow') {
            sta['startShow'] = ''
            sta['stopShow'] = 'none'
            sta['autoShow'] = 'none'
        } else if(dis == 'stopShow') {
            sta['startShow'] = 'none'
            sta['stopShow'] = ''
            sta['autoShow'] = 'none'
        } else {
            sta['startShow'] = 'none'
            sta['stopShow'] = 'none'
            sta['autoShow'] = ''
        }
        this.setState(sta)
    }

    render() {
        let {show} = this.props;
        let {user_width, luck_left, prizeShow, prizeName, prizeNum, amountShow, amountName, amountNum, amountResNum, rightClassName, leftShow, animateShow
            , startShow, stopShow, autoShow, lotteryUserName, lotteryName, lotteryHeadImg, model, modelName, modelShow, data} = this.state;
        let left = luck_left + 'px';
        let width = user_width + 'px';
        /*抽奖用户*/
        let item = this.getItem()
        /*抽奖奖品*/
        let prizeItem = this.genPrize()
        let amountItem = this.genAmount()
        return(
            <div data-moduleid="100105" data-modulename="lottery" className="module lottery" id="lottery" style={{display: show}}>
                <ul className="moduleBtn">
                    <li className="btn_lottery" data-name="lottery" data-description="滚动抽奖"><i className="iconfont">&#xe64a;</i></li>
                </ul>
                <div className="left" style={{display: leftShow}}>
                    <div className="title">
                        <i className="icon icon-title"></i>滚动抽奖（{data.length}人参加）
                    </div>
                    <div className="userList began">
                        <div className="lucky"></div>
                        <div id="luck_user">
                            <ul style={{width: width,left: left}}>
                                {item}
                            </ul>
                        </div>
                    </div>
                    <div className="condition">
                        奖项:
                        <div className="select select-lage select-prize">
                            <div id="prize_div" className="selected" data-prizeid="0" data-amount="0" onClick={this.showPrize}>{prizeName}</div>
                            <ul className="prize" style={{display: prizeShow}}>
                                {prizeItem}
                            </ul>
                        </div>
                        人数:
                        <div className="select select-number">
                            <div id="amount_div" className="selected" data-amount="0" onClick={this.showAmount}>{amountName}</div>
                            <ul id="amount_ul" style={{display: amountShow}}>
                                {amountItem}
                            </ul>
                        </div>
                        抽奖模式:
                        <div className="select select-number">
                            <div id="amount_div" className="selected" onClick={this.showModel}>{modelName}</div>
                            <ul id="amount_ul" style={{display: modelShow}}>
                                <li key="1" onClick={this.seletModel.bind(this, 'Surplus')}>剩余</li>
                                <li key="2" onClick={this.seletModel.bind(this, 'All')}>全员</li>
                            </ul>
                        </div>
                    </div>
                    <div className="btn-box">
                        <a className="clickBtn" id="beginLuck" onClick={this.startLottery} style={{display: startShow}}>开始抽奖</a>
                        <a className="clickBtn stopBtn" id="stopLuck" onClick={this.stopLottery} style={{display: stopShow}}>停止抽奖</a>
                        <a className="clickBtn" id="luckIng" style={{display: autoShow}}>自动抽奖中(<span>{amountResNum}</span>)</a>
                    </div>
                </div>
                {/* 获奖名单 */}
                <div id="name_list" className={rightClassName}>
                    <div className="title"><i className="icon icon-title"></i>中奖名单
                        {/*中奖人数*/}
                        <LotteryLuckNum/>
                        <i id="zk_md" className="icon icon-more" onClick={this.openWinning}></i>
                    </div>
                    {/*中奖名单*/}
                    <LotteryLuckUL/>
                </div>
                {/*中奖动画*/}
                <LotteryAnimate show={animateShow} name={lotteryUserName} prizeName={lotteryName} headImg={lotteryHeadImg}/>
            </div>
        )
    }
}
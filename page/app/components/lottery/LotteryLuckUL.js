import React from 'react';
import LotteryLuck from "./LotteryLuck";
import AsynPost from '../../common/AsynPost.js'
export default class LotteryLuckUL extends React.Component {

    static defaultProps = {

    }

    constructor(props, context) {
        super(props)
        this.state = {
            data: []
        }
    }
    /*获取中中奖名单*/
    componentWillMount() {
        setInterval(function () {
            let data = {}
            AsynPost(data, 'getPrizeUser.html', this.obPrizeUser)
        }.bind(this), 2000)
    }

    initLottery = () => {
        let data = {}
        AsynPost(data, 'initLottery.html', this.obPrizeUser)
    }

    obPrizeUser = (res) => {
        let sta = this.state
        sta['data'] = res.data.list
        this.setState(sta)
    }

    getItem = () => {
        let sta = this.state
        let data = sta.data
        const itemArr = []
        for(let i = 0; i < data.length; i++) {
            let dataItem = data[i]
            let item = (<LotteryLuck key={i} lotteryLevel={dataItem.lotteryLevel} lotteryNum={dataItem.lotteryNum} lotteryName={dataItem.lotteryName} data={dataItem.lottery}/>)
            itemArr.push(item)
        }
        return itemArr
    }

    render() {
        let {} = this.state
        let item = this.getItem()
        return(
            <div>
                <div id="luckUl" className="result">
                    {item}
                </div>
                {/*待筛选功能TODO*/}
                <div className="btn-box">
                    <a className="btn reclick" id="removeLottery" onClick={this.initLottery}>重新抽奖</a>
                    <a className="btn submitUser gray" id="submitLottery">确认名单</a>
                </div>
            </div>
        )
    }
}
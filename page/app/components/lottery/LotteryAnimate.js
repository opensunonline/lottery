import React from 'react';
export default class LotteryAnimate extends React.Component {

    static defaultProps = {
        show: 'none',
        name: '',
        prizeName: ''
    }

    constructor(props, context) {
        super(props)
        this.state = {
            show: 'none'
        }
    }

    componentWillReceiveProps(nextProps){
        let sta = this.state
        sta['show'] = nextProps.show
        this.setState(sta)


    }

    render() {
        let {show} = this.state
        let {name, prizeName, headImg} = this.props
        return(
            <div className="animate-bg" style={{display: show}}>
                <div className="light"></div>
                <div className="lottery-animate-bg">
                    <div className="lotteryuserhead">
                        <img src={headImg} />
                    </div>
                    <div className="level">恭喜<span className="user-name">{name}</span>获得<p className="awards-name">{prizeName}</p>
                    </div>
                </div>
            </div>
        )
    }
}
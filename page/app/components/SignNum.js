import React from 'react';
import AsynPost from '../common/AsynPost.js'
export default class SignNum extends React.Component {

    static defaultProps = {}

    constructor(props, context) {
        super(props)
        this.state = {
            signNumber: 0
        }
    }

    componentWillMount() {
        setInterval(function () {
            let data = {}
            AsynPost(data, 'getSignNum.html', this.callback)
        }.bind(this), 1000)
    }

    callback = (res) => {
        let sta = this.state
        sta['signNumber'] = res.data.num
        this.setState(sta)
    }

    render() {
        const {signNumber} = this.state;
        return(
            <div data-modulename="count" id="joinCount" style={{display: 'block', left: '700px'}}>
                <div className="num" id="userCount"><span>{signNumber}</span>人签到</div>
            </div>
        )
    }
}
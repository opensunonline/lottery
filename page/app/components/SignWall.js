import React from 'react';
import AsynPost from '../common/AsynPost.js'
export default class SignWall extends React.Component {

    static defaultProps = {
        show: 'none'
    }

    constructor(props, context) {
        super(props);
        this.state = {
            data: [],
            walls: [37,20,21,23,24,40,54,55,69,83,97,81,65,49,34,35,36,38,39,50,51,53,66,68,82,52,67]
        }
    }
    /*获取签到用户*/
    componentWillMount() {
        setInterval(function () {
            let data = {}
            AsynPost(data, 'getSignUser.html', this.obSignUser)
        }.bind(this), 1000)
    }

    obSignUser = (res) => {
        let sta = this.state
        sta['data'] = res.data.list
        this.setState(sta)
    }

    getItem = () => {
        const itemArr = [];
        let sta = this.state
        let data = sta.data
        let walls = sta.walls
        for(let i = 0; i < 120; i++) {
            let itemEl = (<li id={`wall_${i+1}`} key={`${i}`} style={{width: '62px', height: '62px'}}></li>)
            for(let k = 0; k < walls.length; k++) {
                let wall = walls[k]
                if(wall == i) {
                    let dataItem = data[k]
                    if(wall == 52) {
                        itemEl = (<li id={`wall_${i+1}`} key={`${i}`} style={{width: '62px', height: '62px'}}><img style={{width: '100%', height: '100%'}} src="images/wo.png"/></li>)
                    } else if(wall == 67) {
                        itemEl = (
                            <li id={`wall_${i + 1}`} key={`${i}`} style={{width: '62px', height: '62px'}}><img style={{width: '100%', height: '100%'}} src="images/men.png"/></li>)
                    } else {
                        if(dataItem != undefined) {
                            itemEl = (<li id={`wall_${i+1}`} key={`${i}`} style={{width: '62px', height: '62px'}}><img style={{width: '100%', height: '100%'}} src={dataItem.headImg}/></li>)
                        }
                    }
                }
            }
            itemArr.push(itemEl)
        }
        return itemArr;
    }

    render() {
        const item = this.getItem()
        let {show} = this.props;
        return(
            <div data-moduleid="100102" data-modulename="messagewall" className="module" id="messagewall" style={{display: show, width: '1010px'}}>
                <ul className="moduleBtn">
                    <li className="btn_messagewall" data-name="messagewall" data-description="消息墙"><i className="iconfont">&#xe63e;</i></li>
                </ul>
                <ul>
                    {item}
                    {/*<div id="userMessage" className="animate_default" style={{display: 'block'}}>*/}
                        {/*<div className="userhead">*/}
                            {/*<img src="http://wx.qlogo.cn/mmopen/Q3auHgzwzM5xuDWUWxvnW6gCibuFE4Iib5UccPQpsqg9ia1cZ35kEVfBkyvugDaE7gK4JcVIxfANCEJXBRAdEM9Lc4iaIbylnkicx92bib0wOtryQ/0"/>*/}
                            {/*<div className="username">Open Suns</div>*/}
                        {/*</div>*/}
                        {/*<div className="userMessage" style={{borderColor: 'rgb(206, 206, 206)', background: 'rgb(255, 255, 255)', fontSize: '90px'}}>*/}
                            {/*<div className="userMessageContent" style={{color:'#000000',opacity:1}}>签到上墙</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                </ul>
                <div id="wall_zz"></div>
            </div>
        )
    }
}
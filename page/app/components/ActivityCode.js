import React from 'react';
export default class ActivityCode extends React.Component {

    static defaultProps = {
        show: 'none'
    }

    constructor(props, context) {
        super(props)
        this.state = {
            show: 'none'
        }
    }

    componentWillReceiveProps(nextProps){
        let sta = this.state
        sta['show'] = nextProps.show
        this.setState(sta)
    }

    closeCode = () => {
        let sta = this.state
        sta['show'] = 'none'
        this.setState(sta)
    }

    openCode = () => {
        let sta = this.state
        sta['show'] = 'block'
        this.setState(sta)
    }

    render() {
        let {show} = this.state;
        return(
            <div id="activityCode">
                <img src="images/qrcode.png" id="top_code" onClick={this.openCode}/>
                <div id="showCode" className="module" style={{display: show}}>
                    <a className="closeBtn" title="关闭" onClick={this.closeCode}>x</a>
                    <span className="title">使用微信扫描以下任意二维码，关注后发送内容即可上墙</span>
                    <img src="images/qrcode.png" className="big"/>
                    <img src="images/qrcode.png" className="middle"/>
                    <img src="images/qrcode.png" className="small"/>
                </div>
            </div>
        )
    }
}
import fetch from 'isomorphic-fetch';
import PublicComponent from '../common/PublicComponent.js';
export default function postJSON(data, url, callback) {
    url=PublicComponent.common.host + url;
    let dataStr = "data=" +JSON.stringify(data)
    fetch(url,{method: 'post',mode:'cors',headers: {
        'Content-Type': 'application/x-www-form-urlencoded;'+'charset=UTF-8'}, body:dataStr})
        .then(resp => {
            if (resp.status >= 200 && resp.status < 300) return resp.json();
            const error = new Error("服务器异常(" + resp.status + ")");
            throw error;
        }).then(json=>{
        callback(json);
    }).catch(e=>{
        callback({"succ":false,"mesg":e.message});
    });
}
